<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('topic');
            $table->string('subject');
            $table->string('words');
            $table->string('academic_level');
            $table->string('sources');
            $table->string('referencing_style');
            $table->string('intructions');
            $table->string('urgency');
            $table->string('currency');
            $table->string('amount');
            $table->string('order');
            $table->string('status');
            $table->string('deadline');
            $table->string('deadline_writers');
            $table->string('preferred_writer');
            $table->string('writer_paid');
            $table->string('writer_pay');
            $table->string('order_period');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
