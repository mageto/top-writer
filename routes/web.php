<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/contact-form', 'Client\ClientController@contact_form');

Route::get('/order-now', function () {
    $subject   =  \DB::table('subjects')->get();
    $academics =  \DB::table('academic_level')->get();
    $urgency   =  \DB::table('custom_prices')->get();
    return view('welcome', compact('subject', 'academics', 'urgency'));
});

Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();
// paypal
Route::view('/checkout', 'checkout-page');
Route::post('/new-order', 'PaymentController@createPayment')->name('create-payment');
// Route::get('/confirm-new-payment/{data}', 'PaymentController@confirmPayment')->name('confirm-new-payment');
// Route::get('/confirm-new-payment', function () {
//     echo var_dump($data['urgency']);
// })->name('confirm-new-payment');
Route::get('/confirm-new-payment', [
    'as' => 'confirm-new-payment',
    'uses' => 'PaymentController@confirmPayment',
    'data' => 'data'
]);
Route::get('/confirm', 'Order\OrderController@confirmPayment')->name('confirm-payment');
// chat
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessageController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessageController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessageController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessageController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessageController@update']);
});

Route::get('/home', 'HomeController@index')->name('home');

// client routes
Route::get('/dashboard', 'Client\ClientController@index');
Route::post('/add-client', 'Client\ClientController@add_client');
Route::get('/get-client/{id}', 'Client\ClientController@get_client');
Route::get('/get-clients', 'Client\ClientController@get_clients');
Route::get('/clients', 'Client\ClientController@clients');
Route::get('/client/{id}', 'Client\ClientController@client');
Route::get('/update-client/{id}', 'Client\ClientController@update_client');
Route::delete('/delete-client/{id}', 'Client\ClientController@delete_client');
Route::post('/rate-order', 'Client\ClientController@rate_order');
// orders
// Route::post('/new-order', 'Order\OrderController@new_order');
Route::post('/add-order', 'Order\OrderController@createPayment');
Route::get('/get-order/{id}', 'Order\OrderController@get_order');
Route::get('/orders', 'Order\OrderController@orders');
Route::get('/update-order/{id}', 'Order\OrderController@update_order');
Route::delete('/delete-order/{id}', 'Order\OrderController@delete_order');
Route::put('/cancel-order/{id}', 'Order\OrderController@cancel_order');
Route::get('/open-orders', 'Writer\WriterController@open_orders');
Route::get('/unpaid', 'Writer\WriterController@unpaid_orders');
Route::get('/assigned-orders', 'Writer\WriterController@assigned_orders');
Route::post('/assign-order', 'Order\OrderController@assign_order');
Route::post('/completed-order', 'Order\OrderController@completed_order');
Route::post('/completed-order-writer', 'Order\OrderController@completed_order_writer');
Route::get('/completed-orders', 'Writer\WriterController@completed_orders');
Route::get('/cancelled-orders', 'Writer\WriterController@canceled_orders');
Route::get('/pending-edition', 'Writer\WriterController@revision_orders');
Route::get('/revision', 'Writer\WriterController@revision_orders');
Route::get('/my-orders', 'Order\OrderController@my_orders');
Route::get('/my-order/{id}', 'Order\OrderController@my_order');
// calculate
Route::get('/calculate', 'Order\OrderController@calculate');
// writers
Route::post('/hire-writer', 'Writer\WriterController@hire_writer');
Route::post('/add-writer', 'Writer\WriterController@add_writer');
Route::post('/writer-files', 'Writer\WriterController@writer_files');
Route::get('/get-writer/{id}', 'Writer\WriterController@get_writer');
Route::get('/writers', 'Writer\WriterController@writers');
Route::get('/writer/{id}', 'Writer\WriterController@writer');
Route::get('/invoice/{id}', 'Writer\WriterController@invoice');
Route::post('/add-invoice', 'Writer\WriterController@add_invoice');
Route::get('/my-invoice', 'Writer\WriterController@invoices');
Route::get('/my-invoice/{id}', 'Writer\WriterController@my_invoice');
Route::get('/get-writers', 'Writer\WriterController@get_writers');
Route::get('/update-writer/{id}', 'Writer\WriterController@update_writer');
Route::delete('/delete-writer/{id}', 'Writer\WriterController@delete_writer');
// messages
Route::get('/email', 'MessageController@email');
// admin-orders
Route::get('/order', 'Admin\AdminController@order');
Route::get('/open', 'Admin\AdminController@open');
Route::get('/assigned', 'Admin\AdminController@assigned');
Route::get('/completed', 'Admin\AdminController@completed');
Route::get('/revisions', 'Admin\AdminController@revisions');
Route::get('/canceled', 'Admin\AdminController@canceled');
Route::get('/profile', 'Admin\AdminController@profile');
Route::post('/profile-update', 'Admin\AdminController@profile_update');
Route::post('/update-avatar', 'Admin\AdminController@update_avatar');

// financials
Route::get('/msg-config', 'HomeController@msg_config');
Route::post('/email-config', 'HomeController@email_config');
Route::get('/financial', 'HomeController@financial');

// charts
Route::get('/first-chart', 'Admin\AdminController@first_chart');
