@extends('layouts.app')

@section('content')
<div class="card-box col-md-offset-2 col-md-8">   
    @include('messenger.partials.flash')

    @each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
</div>
@stop