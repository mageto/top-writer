<!-- <h2>Add a new message</h2> -->
<form action="{{ route('messages.update', $thread->id) }}" method="post">
    {{ method_field('put') }}
    {{ csrf_field() }}

    <!-- <div class="col-md-4">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default bx-shadow-none">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Choose User
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?php if ( Auth::user()->user_type == 'admin') { ?>
                            @if($users->count() > 0)
                                <div class="checkbox">
                                    @foreach($users as $user)
                                        <label title="{{ $user->name }}">
                                            <input type="checkbox" name="recipients[]" value="{{ $user->id }}">{!!$user->name!!}
                                        </label>
                                <br>
                                    @endforeach
                                </div>
                            @endif
                        <?php } if ( Auth::user()->user_type == 'client')  { ?>
                            @if($users->count() > 0)
                                <div class="checkbox">
                                    @foreach($admin_users as $user)
                                        <label title="{{ $user->name }}">
                                            <input type="checkbox" name="recipients[]" value="{{ $user->id }}">{!!$user->name!!}
                                        </label>
                                    @endforeach
                                </div>
                            @endif
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="col-md-8">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default bx-shadow-none">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Add a new message
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <!-- Message Form Input -->
                        <div class="form-group">
                            <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                        </div>
                        <!-- Submit Form Input -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-control">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>