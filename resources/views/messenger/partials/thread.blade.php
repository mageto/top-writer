<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>

<div class="media alert ">
    <div class="col-md-">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel {{ $class }} panel-default bx-shadow-none">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a class="" role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">                    
                            <h4 class="media-heading">
                                <a href="{{ route('messages.show', $thread->id) }}">
                                    {{ $thread->subject }}
                                </a>
                                ({{ $thread->userUnreadMessagesCount(Auth::id()) }} Unread)
                            </h4>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <p>
                            {{ $thread->latestMessage->body }}
                        </p>
                        <p>
                            <small><strong>Creator:</strong> {{ $thread->creator()->name }}</small>
                        </p>
                        <p>
                            <small><strong>Participants:</strong> {{ $thread->participantsString(Auth::id()) }}</small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


        <!-- <div class="col-md-6">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default bx-shadow-none">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div> -->