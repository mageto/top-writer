@extends('layouts.app')

@section('content')
<div class="col-md-12 ard-box">  
    <div class="col-md-8 col-md-offset-2">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default bx-shadow-none">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h1>{{ $thread->subject }}</h1>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        @each('messenger.partials.messages', $thread->messages, 'message')
                    <hr>
                        @include('messenger.partials.form-message')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

<div class="col-md-6">
        </div>