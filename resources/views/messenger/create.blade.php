@extends('layouts.app')

@section('content')

<div class="card-box col-md-8 col-md-offset-2">  

    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
        <div class="col-md-4">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default bx-shadow-none">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Choose User
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <?php if ( Auth::user()->user_type == 'admin') { ?>
                                @if($users->count() > 0)
                                    <div class="checkbox">
                                        @foreach($users as $user)
                                            <div style="margin-top:-5px; margin-bottom:-5px;" class="checkbox checkbox-primary">
                                                <input id="checkbox3" type="checkbox" name="recipients[]" value="{{ $user->id }}">
                                                <label for="checkbox3" title="{{ $user->name }}">
                                                    {!!$user->name!!}
                                                </label>
                                            </div>
                                    <br>
                                        @endforeach
                                    </div>
                                @endif
                            <?php } if ( Auth::user()->user_type == 'client')  { ?>
                                @if($users->count() > 0)
                                    <div class="checkbox">
                                        @foreach($admin_users as $user)
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox3" type="checkbox" name="recipients[]" value="{{ $user->id }}">
                                                <label for="checkbox3" title="{{ $user->name }}">
                                                    {!!$user->name!!}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default bx-shadow-none">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Create a new message
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <!-- Subject Form Input -->
                            <div class="form-group">
                                <label class="control-label">Subject</label>
                                <input type="text" class="form-control" name="subject" placeholder="Subject"
                                    value="{{ old('subject') }}">
                            </div>

                            <!-- Message Form Input -->
                            <div class="form-group">
                                <label class="control-label">Message</label>
                                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                            </div>
                            
                            <!-- Submit Form Input -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary form-control">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop