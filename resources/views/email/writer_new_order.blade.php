<h4>Dear {{ $name }}, </h4>
<p>You have been assigned a new order, {{ $order_no }}, kindly log on to your account on {{ $site_url }}to view it.</p>
<p>Regards,<br />
Site Admin.</p>
