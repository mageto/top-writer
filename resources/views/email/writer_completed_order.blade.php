<h4>Dear Site Admin, </h4>
<p>The order, {{ $order_no }}, has been marked as completed by the writer.<br/>
Kindly log on {{ $site_url }}, to view it.</p>
<p>Regards,<br />
Site Admin.</p>
