<style>
table, td, th {  
  border: 1px solid #ddd;text-align: left;
}

table {
  border-collapse: collapse; width: 100%;
}

th, td {
  padding: 10px;
}
tr:nth-child(even) {background-color: #f2f2f2;}
</style>
<h4>Dear Site Admin, </h4>
<p>{{ $mailmessage }}</p>

<div class="col-lg-6">
    <div class="card-box">
        <h4 class="header-title m-t-0 m-b-30">{{ $client_id }}, {{ $name }} : Order details</h4>
        <h4 class="header-title m-t-0 m-b-30">Topic : {{ $topic }}</h4>

        <h4> Order Number :{{ $orderNo }} </h4>
        <p class="text-muted font-13 m-b-15">
            <strong>Instructions : </strong>{{$instructions}}
        </p>

        <table style="border: 1px solid #ddd;text-align: left;border-collapse: collapse; width: 50%;" class="table m-b-0">
            <thead>
                <tr>
                    <th style="padding: 10px;">#</th>
                    <th style="padding: 10px;">Details</th>
                </tr>
            </thead>
            <tbody>
                <tr style="background-color: #f2f2f2;">
                    <th style="padding: 10px;" scope="row">Order Topic</th>
                    <td style"padding: 10px;">{{$topic}}</td>
                </tr>
                <tr style="background-color: #fff;">
                    <th style="padding: 10px;" scope="row">Subject</th>
                    <td style"padding: 10px;">{{$subject}}</td>
                </tr>
                <tr style="background-color: #f2f2f2;">
                    <th style="padding: 10px;" scope="row">Academic Level</th>
                    <td style"padding: 10px;">{{$academic_level}}</td>
                </tr>
                <tr style="background-color: #fff;">
                    <th style="padding: 10px;" scope="row">Words/Pages</th>
                    <td style"padding: 10px;">{{$words}}</td>
                </tr>
                <tr style="background-color: #f2f2f2;">
                    <th style="padding: 10px;" scope="row">Urgency</th>
                    <td style"padding: 10px;">{{$urgency}}</td>
                </tr>
                <tr style="background-color: #fff;">
                    <th style="padding: 10px;" scope="row">Order Status</th>
                    <td style"padding: 10px;">{{$status}}</td>
                </tr>
                <tr style="background-color: #fff;">
                    <th style="padding: 10px;" scope="row">Referencing Style</th>
                    <td style"padding: 10px;">{{$referencing_style}}</td>
                </tr>
                <tr style="background-color: #f2f2f2;">
                    <th style="padding: 10px;" scope="row">Sources</th>
                    <td style"padding: 10px;">{{$sources}}</td>
                </tr>
                <tr style="background-color: #fff;">
                    <th style="padding: 10px;" scope="row">Order Deadline</th>
                    <td style"padding: 10px;">{{$deadline}}</td>
                </tr>
                <tr style="background-color: #f2f2f2;">
                    <th style="padding: 10px;" scope="row">Amount</th>
                    <td style"padding: 10px;">{{$amount}}</td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
<p>Regards,<br />
Site Admin.</p>
