@extends('layouts.app')

@section('content')
    
                <!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
            <div class="pull-right">
                <a href="#custom-modal" class="btn btn-primary waves-effect waves-light m-r-5 m-b-10" data-animation="door" data-plugin="custommodal"
                                data-overlaySpeed="100" data-overlayColor="#36404a">Add Writer</a>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable-buttons" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($writers as $value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{$value->phone}}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="/writer/{{$value->id}}">View</a>
                            <a class="btn btn-sm btn-danger" onclick="delete_writer({{$value->id}})">Delete</a>
                            <a class="btn btn-sm btn-inverse" href="/invoice/{{$value->id}}">Pay</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Writer</h4>
    <div class="custom-modal-text">              
        <form class="form-horizontal" role="form">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone">
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-6 col-sm-9">
                    <button type="button" onclick="add_writer()" class="btn btn-info waves-effect waves-light">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function edit_writer(writer_id){

    $.get('/get-writer/' + writer_id, function (data) {

        $('#btn-save').val("Update");
        console.log(data);

        $('#writerId').val(data[0].id).change();
        $('#amount').val(data[0].amount);
        $('#transactionRef').val(data[0].transactionRef);
        $('#datePaid').val(data[0].datePaid);
    });
    }

    function add_writer(){
        var name   =  document.getElementById('name').value;
        var email  =  document.getElementById('email').value;
        var phone  =  document.getElementById('phone').value;
        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-writer', // point to server-side PHP script
                data: { name : name, email : email, phone : phone } ,
                type: 'POST',
                success: function(data) {

                        document.getElementById('name').val = "";
                        document.getElementById('email').val = "";
                        document.getElementById('phone').val = "";

                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#custom-modal').modal('hide');
                        toastr.success('writer added successfully.');

                },
                error: function (error, data) {
                    console.log('Error:', data);
                }
            });
    }


    function delete_writer(id){
        swal({
            title: "Are you sure,",
            text: "You want to delete this Writer!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            },
            function(){
                // ajax delete data to database
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : "delete-writer/"+id,
                    type: "DELETE",
                    dataType: "JSON",
                    success: function(data)
                    {
                        swal({   title: "Deleted",   
                            text: "Writer has been Deleted.",   
                            timer: 1000,
                            type: "success",   
                            showConfirmButton: false 
                        });
                        location.reload();
                    },
                });
            });
    }
</script>

@endsection