@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Client</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Topic</th>
                <th>Academic Level</th>
                <th>Date Posted</th>
                <th>Pay</th>
                <th>Paid</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($orders as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->clients['name']}}</td>
                <td>{{$value->client['email']}}</td>
                <td>{{$value->subjects['subject']}}</td>
                <td>{{$value->topic}}</td>
                <td>{{$value->academic_level}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->writer_pay}}</td>
                <td>{{$value->writer_paid}}</td>
                <td>{{$value->status}}</td>
                <td>
                    <a class="btn btn-xs btn-primary" href="/my-order/{{$value->id}}">View</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection