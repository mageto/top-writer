@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <!-- <div class="panel-heading">
                <h4>Invoice</h4>
            </div> -->
            <div class="panel-body">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="logo invoice-logo"><img src="{{ asset('img/logo.png') }}" alt="Logo" style="height:50px;"></h3>
                    </div>
                    <div class="pull-right">
                        <h4>Invoice # <br>
                            <strong>{{$invoice_no}}</strong>
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left m-t-30">
                            <address>
                                @foreach($writer as $key)
                                <strong>{{$key->name}}</strong><br>
                                {{$key->email}}<br>
                                {{$key->address}}<br>
                                <abbr title="Phone">P:</abbr> {{$key->phone}}
                                @endforeach
                            </address>
                        </div>
                        <div class="pull-right m-t-30">
                            <p><strong>Order Date: </strong> {{date('Y:m:d')}} </p>
                            <p class="m-t-10"><strong>Invoice Status: </strong> <span class="label label-pink">Approved</span></p>
                            <p class="m-t-10"><strong>Invoice Number: </strong> {{$invoice_no}}</p>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <div class="m-h-50"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table m-t-30">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Academic Level</th>
                                        <th>Page(s)</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($invoice as $value)
                                    <tr>
                                        <td>{{ $value->id }}</td>
                                        <td>{{ $value->title }}</td>
                                        <td>{{ $value->academic_level }}</td>
                                        <td>{{ $value->words }}</td>
                                        <td>{{ $value->status }}</td>
                                        <td>{{ $value->writer_pay }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="clearfix m-t-40">
                            <h5 class="small text-inverse font-600">PAYMENT</h5>

                            <small>
                                I certify that the above orders have been done and completed efficiently.
                            </small>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-2">
                        <p class="text-right"><b>Sub-total:</b> {{ $total }}</p>
                        <!-- <p class="text-right">Discout: 12.9%</p>
                        <p class="text-right">VAT: 12.9%</p> -->
                        <hr>
                        <h3 class="text-right">USD {{ $total }}</h3>
                    </div>
                </div>
                <hr>
                <div class="hidden-print">
                    <div class="pull-right">
                        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                        <button class="btn btn-primary waves-effect waves-light"onclick="add_invoice()">Approve</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="invoice_no" value="{{$invoice_no}}">
<input type="hidden" id="writer_id" value="{{$writer_id}}">
<input type="hidden" id="orders" value="{{$orders}}">
<input type="hidden" id="total" value="{{$total}}">
<script>
    function add_invoice(){
        var invoice_no  = document.getElementById('invoice_no').value;
        var writer_id   = document.getElementById('writer_id').value;
        var orders      = document.getElementById('orders').value;
        var total       = document.getElementById('total').value;
    swal({
        title: "Are you sure,",
        text: "You want to add this invoice!",
        type: "success",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, add it!",
        closeOnConfirm: false
        },
        function(){
            // ajax delete data to database
                $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : "/add-invoice",
                type: "POST",
                data: { invoice_no : invoice_no, writer_id : writer_id, orders : orders, total : total } ,
                success: function(data)
                {
                    swal({   title: "Invoice",   
                        text: "Invoice has been Added.",   
                        timer: 1000,
                        type: "success",   
                        showConfirmButton: false 
                    });
                    location.reload();
                },
            });
        });
    }
</script>
@endsection
