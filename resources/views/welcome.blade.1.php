<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Top Writers</title>

    <link href="{{ asset('landing/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/fontawesome-all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/nav-menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/mailform.css') }}" rel="stylesheet" type="text/css" />
    <!-- Styles -->
    <!-- form Uploads -->
    <!-- <link href="{{ asset('assets/plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" /> -->

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('css/app.css') }}') }}" rel="stylesheet">-->

    <!-- plugins -->
    <!-- <link rel="stylesheet" href="{{ asset('landing/plugins.css') }}" /> -->

    <!-- search css -->
    <link rel="stylesheet" href="{{ asset('landing/search.css') }}" />

    <!-- switcher css -->
    <link href="{{ asset('landing/switcher.css') }}" rel="stylesheet">

    <!-- core style css -->
    <link href="{{ asset('landing/styles.css') }}" rel="stylesheet" id="colors" />

        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>

</head>
<body>

<!-- start page loading -->
<div id="preloader">
    <div class="row loader">
        <div class="loader-icon"></div>
    </div>
</div>
<!-- end page loading -->

<!-- start main-wrapper section -->
<div class="main-wrapper wrapper-boxed">

    <!-- Start Header -->
    <header class="header onepage-header" data-scroll-index="0">

        <div class="container">

            <div class="menu_area alt-font">

                <!-- Start Navbar -->
                <nav style="background:#6d75eb;" class="navbar navbar-expand-lg no-padding">

                    <div class="container sm-position-relative">

                        <div class="navbar-header navbar-header-custom">
                            <!-- start logo -->
                            <a href="javascript:void(0)" data-scroll-nav="0" class="navbar-brand white-logo"><img id="logo" src="http://www.chitrakootweb.com/template/crysta/img/logos/logo-white.png" alt="logo"></a>
                            <!-- end logo -->
                        </div>

                        <div class="navbar-toggler"></div>

                        <ul class="navbar-nav ml-auto" id="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="/" data-scroll-nav="0">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/#1" data-scroll-nav="1">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/#2" data-scroll-nav="2">Services</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/#3" data-scroll-nav="3">Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/#6" data-scroll-nav="6">Contact</a>
                            </li>
                        </ul>

                        <div class="margin-20px-left sm-display-none">
                            <a class="butn white" href="/login">Login</a>
                        </div>
                        <div class="margin-20px-left sm-display-none">
                            <a class="butn white" href="/order-now">Order Now</a>
                        </div>

                    </div>

                </nav>
                <!-- End Navbar  -->
            </div>

        </div>

    </header>
    <!-- End Header -->

<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- <div class="card-box">  -->
              <div class="col-md-12 card-box">
                  <div class="main border-right">
                    <h3>New order</h3>
                    <hr/>
                    <div class="col-md-7">
                      <form class="form-horizontal" action="/new-order" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="title">Title</label>
                          <div class="col-sm-9">
                            <input type="text" name="topic" class="form-control form" id="title" placeholder="Enter topic" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="subject">Subject (category)</label>
                          <div class="col-sm-9 select">          
                            <select name='subject' id='subject' class="form-control" required >
                              <option value=""> Select Subject </option>
                              <?php foreach ($subject as $key) { ?>
                                <option value={{ $key->id }}>{{ $key->subject }}</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="academiclevel">Academic level</label>
                          <div class="col-sm-9 select">          
                            <select id='academic_level' name='academic_level' class="form-control"   required >
                              <option value=""> Select Academic Level </option>
                              <?php foreach ($academics as $key) { ?>
                                <option value={{ $key->academic_level }}>{{ $key->name }}</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="words">Word/Pages</label>
                          <div class="col-sm-9 select">          
                            <select id="words" name='words'  class="form-control"   required >
                              <?php for ($x = 1; $x <= 100; $x++) {  ?>
                                <option value="<?php echo $x; ?>"> <?php echo $x; ?> Pages || Aprox <?php $words=$x*275; echo $words; ?> Words </option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="urgency">Urgency</label>
                          <div class="col-sm-9 select">          
                              <select  id='urgency' name='urgency' class="form-control" required>
                                <?php foreach ($urgency as $key) { ?>
                                  <option style="text-transform:capitalize;" value="{{ $key->deadline }}">{{ $key->deadline }}</option>
                                <?php } ?>
                              </select>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="amount">
                          <label class="control-label col-sm-4 text-right" for="">Amount </label>
                          <div class="col-sm-2 select">
                            <select  id='currency' name='currency' class="form-control " >
                                    <option value="1">USD</option>
                                    <option value="0.89">EUR</option>
                                    <option value="1.3">AUD</option>
                                    <option value="0.69">GBP</option>
                                  </select>

                          </div>
                          <div class="col-sm-5">
                            <input type="input" id="amount" class="form-control" name="amount" value="$" placeholder="10" readonly/>
                            <input type="hidden" id="usdamount" class="form-control" name="usdamount" value="$" placeholder="10" readonly/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="pwd">Number of Sources</label>
                        <div class="col-sm-9 select">          
                          <select id='sources' name='sources'  class="form-control"  >
                            <option value="1"> Select number of Sources </option>
                            <?php for ($x = 1; $x <= 100; $x++) {  ?>
                            <option value="<?php echo $x; ?>"> <?php echo $x; ?>  </option>
                            <?php } ?> 
                          </select>
                        </div>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="pwd">Format style</label>
                        <div class="col-sm-9 select">          
                          <select name='referencing_style' class="form-control"  >
                            <option> Select Referencing Style </option>
                            <option value="MLA">MLA</option>
                            <option value="HAVARD">HAVARD</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="instructions">Instructions</label>
                        <div class="col-sm-9">
                          <textarea class="ckeditor form-control" id='editor'  name="instructions"></textarea>
                        </div>
                      </div>
                      <hr/>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="name">Name</label>
                          <div class="col-sm-9">
                            <input type="text" name="name" class="form-control form" id="name" placeholder="Enter Name" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="email">Email</label>
                          <div class="col-sm-9">
                            <input type="email" name="email" class="form-control form" id="email" placeholder="Enter Email" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="password">password</label>
                          <div class="col-sm-9">
                            <input type="password" name="password" class="form-control form" id="password" placeholder="Enter password" required>
                          </div>
                        </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="email">Additional materials</label>
                        <div class="col-sm-9">
                          <button id="ADDFILE" class="btn btn-danger fullwidth"> Add/upload file(s)</button>
                          <div id="uploadFileContainer"></div>
                        </div>
                      </div>
                      <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-9">
                          <input type="submit"  class="btn btn-info fullwidth" name="submit"/>
                        </div>
                      </div>
                  </form>
                  </div>
                  <div class="col-md-5">
                    <!-- <div class="col-sm-4"> -->
                    <h3> 100% Custom in 3-steps </h3>
                    <div class="row" style="border: 1px dotted #31b0d5;">
                      <div class="col-sm-3"><h1><i class="fa fa-book fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Fill Order Details</h5><p>  Fill In the Instructions of Your Essays.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-pencil fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Sign Up</h5><p> Sign Up for New clients/Sign In for our regular clients.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-check    fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Confirm</h5><p> Confirm Order is Correct.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-paypal fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Pay for your Order</h5><p>Pay via Paypal/Credit Card.</p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2019 © Top-Writers.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->
        </div>
    </div>
    </div>
    <!-- end main-wrapper section -->

    <!-- start switcher section -->
    <div id="style-switcher">
        <a href="javascript:void(0)" class="switcher-setting"><i class="fa fa-cog fa-spin"></i></a>

        <div class="layout-btn">
            <h2>Select Layout</h2>
            <ul>
                <li><a class="boxed" href="#"><span>Boxed</span></a></li>
                <li><a class="wide" href="#"><span>Wide</span></a></li>
            </ul>
        </div>

        <div class="pattren">

            <div class="pattren-wrap">
                <h2>Chose Pattren</h2>
                <ul class="bg-list">
                    <li>
                        <a href="javascript:void(0)" class="bg1"><img alt="" src="http://www.chitrakootweb.com/template/crysta/img/pattern/pattren1.jpg"></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="bg2"><img alt="" src="http://www.chitrakootweb.com/template/crysta/img/pattern/pattren2.jpg"></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="bg3"><img alt="" src="http://www.chitrakootweb.com/template/crysta/img/pattern/pattren3.jpg"></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="bg4"><img alt="" src="http://www.chitrakootweb.com/template/crysta/img/pattern/pattren4.jpg"></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="bg5"><img alt="" src="http://www.chitrakootweb.com/template/crysta/img/pattern/pattren5.jpg"></a>
                    </li>

                </ul>

            </div>
        </div>

        <div class="choose-color">
            <h2>Choose Color</h2>
            <ul class="colors" id="color1">
                <li>
                    <a href="javascript:void(0)" class="style1"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style2"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style3"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style4"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style5"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style6"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style7"></a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="style8"></a>
                </li>
            </ul>
        </div>

        <div class="choose-demo">
            <h2>Choose Demo</h2>
            <ul class="demo-btn">
                <li>
                    <a href="http://www.chitrakootweb.com/template/crysta/demo-1.html"><span>Demo 01</span></a>
                </li>
                <li>
                    <a href="http://www.chitrakootweb.com/template/crysta/demo-2.html"><span>Demo 02</span></a>
                </li>
                <li>
                    <a href="demo-3.html"><span>Demo 03</span></a>
                </li>
            </ul>
        </div>

    </div>
    <!-- end switcher section -->

    <!-- start scroll to top -->
    <a href="javascript:void(0)" class="scroll-to-top"><i class="fas fa-angle-up" aria-hidden="true"></i></a>
    <!-- end scroll to top -->

    <!-- all js include start -->

    <!-- Java script -->
    <script src="{{ asset('landing/core.min.js') }}"></script>

    <!-- serch -->
    <script src="{{ asset('landing/search.js') }}"></script>
    
    <!-- custom scripts -->
    <script src="{{ asset('landing/main.js') }}"></script>

    <!-- contact form scripts -->
    <script src="{{ asset('landing/jquery.form.min.js') }}"></script>
    <script src="{{ asset('landing/jquery.rd-mailform.min.c.js') }}"></script>

    <!-- all js include end -->


<script type="text/javascript">


  $("select[id='words'], select[id='academic_level'], select[id='subject'], input[id='discount'], select[id='currency'], select[id='urgency']").change(function(){

    $q1 = document.getElementById('urgency').value;
    $q2 = document.getElementById('academic_level').value;
    $q3 = document.getElementById('words').value;
    $q4 = document.getElementById('currency').value;
    // alert($q2);
       $.ajax({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        url : "/calculate",
        type: "get",
        data: {"urgency" : $q1, "academic_level" : $q2, "words" : $q3, "currency" : $q4},
        // dataType: "json",
        success: function(data)
        {
          console.log(JSON.parse(data));
          var dat = JSON.parse(data);
            console.log(dat);
            console.log(dat[0][0].highschool);
            console.log(dat[1].words);
            console.log(dat[2].currency);

            var h = dat[0][0].highschool;
            var m = dat[0][0].master;
            var u = dat[0][0].undergraduate;
            var d = dat[0][0].doctoral;
            var w = dat[1].words;
            var c = parseFloat(dat[2].currency);
            if (h) {
            document.getElementById('amount').value = h*w*c;
            }
            if (m) {
            document.getElementById('amount').value = m*w*c;
            }
            if (u) {
            document.getElementById('amount').value = u*w*c;
            }
            if (d) {
            document.getElementById('amount').value = d*w*c;
            }
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            console.log(errorThrown);
           }
       });
});

</script>

<script type="text/javascript">
  jQuery(document).ready(function($){

    $(document).on('click', 'button#ADDFILE', function(event) {
      event.preventDefault();
      $("div#submit").css("display", "block")
      addFileInput();
    });

    function addFileInput() {
      var html ='';
      html +='<div class="alert alert-info">';
      html +='<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;</button>';
      html +='<strong> Upload file </strong>';
      html +='<input type="file" name="multipleFiles[]">';
      html +='<input type="hidden" name="upload_type" value="material">';
      html +='</div>';

      $("div#uploadFileContainer").append(html);
    }

  });

</script>


<script>
  var createAllErrors = function() {
    var form = $( this ),
    errorList = $( "ul.errorMessages", form );

    var showAllErrorMessages = function() {
      errorList.empty();

// Find all invalid fields within the form.
var invalidFields = form.find( ":invalid" ).each( function( index, node ) {

// Find the field's corresponding label
var label = $("label[for='"+$(this).attr('id')+"']");
    // Opera incorrectly does not fill the validationMessage property.
    message = node.validationMessage || 'Invalid value.';

    errorList
    .show()
    .append( "<li><span>" + "<strong>" + label.html() + "</strong>"+ "</span> " + "Please fill this" + "</li>" );
  });
};

// Support Safari
form.on( "submit", function( event ) {
  if ( this.checkValidity && !this.checkValidity() ) {
    $( this ).find( ":invalid" ).first().focus();
    event.preventDefault();
  }
});

$( "input[type=submit], button:not([type=button])", form )
.on( "click", showAllErrorMessages);

$( "input", form ).on( "keypress", function( event ) {
  var type = $( this ).attr( "type" );
  if ( /date|email|month|number|search|tel|text|select|time|url|week/.test ( type )
    && event.keyCode == 13 ) {
    showAllErrorMessages();
}
});
};

$( "form" ).each( createAllErrors );
</script>


    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>

        <!-- file uploads js -->
        <script src="{{ asset('assets/plugins/fileuploads/js/dropify.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>

        <script type="text/javascript">
            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop a file here or click',
                    'replace': 'Drag and drop or click to replace',
                    'remove': 'Remove',
                    'error': 'Ooops, something wrong appended.'
                },
                error: {
                    'fileSize': 'The file size is too big (1M max).'
                }
            });
        </script>
</body>
</html>
