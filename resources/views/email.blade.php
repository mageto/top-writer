@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Email</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="writer" class="col-sm-3 control-label">Writer</label>
                            <div class="col-sm-9">
                                <select class="form-control select2" name="user_id" id="user_id">
                                    @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}, {{$user->email}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="col-sm-3 control-label">Message</label>
                            <div class="col-sm-9">
                                <textarea id="textarea" class="form-control" maxlength="225" rows="2" placeholder="This textarea has a limit of 225 chars."></textarea>
                            </div>
                        </div>
                        
                        
                        <div class="form-group m-b-0">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit"class="btn btn-info waves-effect waves-light btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
