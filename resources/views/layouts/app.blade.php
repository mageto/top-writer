<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Writers Web') }}</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />

    <!-- Sweet Alert css -->
    <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />

    <!--Chartist Chart CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">

    <!-- Custom box css -->
    <link href="{{ asset('assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <!-- form Uploads -->
    <link href="{{ asset('assets/plugins/fileuploads/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/select2/dist/css/select2-bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('css/app.css') }}') }}" rel="stylesheet"> -->

        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>

</head>
<body>
    <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo"><img src="{{ asset('img/logo.png') }}" alt="Logo" style="height:50px;"></a>
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="dropdown user-box">
                                <a href="#" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                                    @if(Auth::user()->avatar != null)
                                    <img src="{{asset('avatar-files/')}}/{{Auth::user()->avatar}}" alt="user-img" class="img-circle user-img">
                                    @else
                                    <img src="{{ asset('img/user.png') }}" alt="user-img" class="img-circle user-img">
                                    @endif
                                    <div class="user-status away"><i class="zmdi zmdi-dot-circle"></i></div>
                                </a>

                                <ul class="dropdown-menu">
                                    <a class="btn btn-success btn-block">{{ Auth::user()->name }}</a>
                                    <li><a href="profile"><i class="ti-user m-r-5"></i> Profile</a></li>
                                    <!-- <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li> -->
                                    <li><a href="{{ route('logout') }}"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <!-- writer -->
                            <?php if ( Auth::user()->user_type == 'writer') { ?>
                            <li><a href="/dashboard" class="active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a></li>
                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-invert-colors"></i><span> Orders </span> </a>
                                <ul class="submenu">
                                    <li><a href="/assigned-orders"> Assigned Orders </a></li>
                                    <li><a href="/pending-edition"> Pending Edition </a></li>
                                    <li><a href="/completed-orders">Completed Orders </a></li>
                                    <li><a href="/revision"> Revision </a></li>
                                    <li><a href="/cancelled-orders">Canceled Orders </a></li>
                                </ul>
                            </li><li><a href="/open-orders"><i class="zmdi zmdi-invert-colors"></i> <span> Open Orders ({{App\Order::where('status', 'Open')->count()}})</span> </a></li>
                            <li><a href="/unpaid"><i class="zmdi zmdi-chart"></i><span> Unpaid ({{App\Order::where('writer_id', Auth::user()->id)->where('writer_paid', 'unpaid')->count()}})</span> </a></li>
                            <li><a href="/financial"><i class="zmdi zmdi-layers"></i><span>Financial  </span> </a></li>
                            <li><a href="/my-invoice"><i class="zmdi zmdi-layers"></i><span>Invoice  </span> </a></li>
                            <!-- client -->
                            <?php } if ( Auth::user()->user_type == 'client')  { ?>
                            <li><a href="/dashboard" class="active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a></li>
                            <!-- <li><a href="/hire-writer"><i class="zmdi zmdi-invert-colors"></i> <span> Hire a Writer </span> </a></li> -->
                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-invert-colors"></i><span> Orders </span> </a>
                                <ul class="submenu">
                                    <li><a href="/my-orders"> My Orders  </a></li>
                                    <li><a href="/open-orders"> Open Orders  </a></li>
                                    <li><a href="/completed-orders"> Completed Orders  </a></li>
                                    <li><a href="/cancelled-orders">Canceled Orders   </a></li>
                                </ul>
                            </li><!-- <li><a href="/messages"><i class="zmdi zmdi-view-list"></i> <span> Messages </span> </a></li> -->
                            <li><a href="/unpaid"><i class="zmdi zmdi-chart"></i><span> Unpaid ({{App\Order::where('client_id', Auth::user()->id)->where('amount', null)->count()}})</span> </a></li>
                            <li><a href="/revision"><i class="zmdi zmdi-collection-item"></i><span> Revision  ({{App\Order::where('client_id', Auth::user()->id)->where('status', 'revision')->count()}})</span> </a></li>
                            <li><a href="/messages">Messages @include('messenger.unread-count')</a></li>
                            <li><a href="/messages/create">Create New Message</a></li>
                            <li><a href="/financial"><i class="zmdi zmdi-layers"></i><span>Financial  </span> </a></li>
                            <!-- Admin -->
                            <?php } if ( Auth::user()->user_type == 'admin')  { ?>
                            <li><a href="/dashboard" class="active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a></li>
                            <!-- <li><a href="/orders"><i class="zmdi zmdi-invert-colors"></i> <span> Orders </span> </a></li> -->
                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-invert-colors"></i><span> Orders </span> </a>
                                <ul class="submenu">
                                    <li><a href="/orders">All Orders</a></li>
                                    <li><a href="/open">Open Orders</a></li>
                                    <li><a href="/assigned">Assigned Orders</a></li>
                                    <li><a href="/completed">Completed Orders</a></li>
                                    <li><a href="/revisions">Orders for Revision</a></li>
                                    <li><a href="/canceled">Canceled Orders</a></li>
                                </ul>
                            </li>
                            <li><a href="/writers"><i class="zmdi zmdi-collection-text"></i><span> Writers </span> </a></li>
                            <li><a href="/clients"><i class="zmdi zmdi-view-list"></i> <span> Clients </span> </a></li>
                            <li><a href="/email"><i class="zmdi zmdi-chart"></i><span> Send Email </span> </a></li>
                            <li><a href="/msg-config"><i class="zmdi zmdi-collection-item"></i><span> Msg Config </span> </a></li>
                            <li><a href="/financial"><i class="zmdi zmdi-layers"></i><span>Financial  </span> </a></li>
                            <li><a href="/my-invoice"><i class="zmdi zmdi-layers"></i><span>Invoice  </span> </a></li>
                            <li><a href="/messages">Messages @include('messenger.unread-count')</a></li>
                            <li><a href="/messages/create">Create New Message</a></li>
                            <?php } ?>
                        </ul>
                        <!-- End navigation menu  -->
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container">
            
                <?php if ( Auth::user()->user_type == 'client') { ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <a href="/order" class="btn btn-custom dropdown-toggle waves-effect waves-light">Order Now <span class="m-l-5"><i class="fa fa-opencart"></i></span></a>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row m-t-20">
                    <div class="col-lg-12">
                        <!-- <div class="card-box">    -->
                            @yield('content')
                        <!-- </div> -->
                    </div><!-- end col -->

                </div>
                <!-- end row -->


                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2018 © Top-Writers.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->
        </div>
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>

        <!-- Sweet Alert js -->
        <script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
        <script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>


        <!-- file uploads js -->
        <script src="{{ asset('assets/plugins/fileuploads/js/dropify.min.js') }}"></script>

        <script src="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
        <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>

        <!-- Datatables-->
        <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>

        <!-- Datatable init js -->
        <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

        <!-- Page specific only -->
        <script src="{{ asset('assets/pages/jquery.inbox.js') }}"></script>

        <!-- Modal-Effect -->
        <script src="{{ asset('assets/plugins/custombox/dist/custombox.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/custombox/dist/legacy.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').dataTable();
                $('#datatable-keytable').DataTable( { keys: true } );
                $('#datatable-responsive').DataTable();
                $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            } );
            TableManageButtons.init();

        </script>
        <script type="text/javascript">
            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop a file here or click',
                    'replace': 'Drag and drop or click to replace',
                    'remove': 'Remove',
                    'error': 'Ooops, something wrong appended.'
                },
                error: {
                    'fileSize': 'The file size is too big (1M max).'
                }
            });
            
                // Select2
                $(".select2").select2();

        </script>
</body>
</html>
