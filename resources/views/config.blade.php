@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Set New Email</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="post" action="/email-config">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="writer" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text"class="form-control" name="name" id="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="writer" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text"class="form-control" name="email" id="email">
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit"class="btn btn-info waves-effect waves-light btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
