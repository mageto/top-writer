@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <!-- <div class="panel-heading">
                <h4>Invoice</h4>
            </div> -->
            @foreach($invoice as $key)
            <div class="panel-body">
                <div class="clearfix">
                    <div class="pull-left">
                        <h3 class="logo invoice-logo"><img src="{{ asset('img/logo.png') }}" alt="Logo" style="height:50px;"></h3>
                    </div>
                    <div class="pull-right">
                        <h4>Invoice # <br>
                            <strong>{{$key->invoice_no}}</strong>
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left m-t-30">
                            <address>
                                <strong>{{$key->writers['name']}}</strong><br>
                                {{$key->writers['email']}}<br>
                                {{$key->writers['address']}}<br>
                                <abbr title="Phone">P:</abbr> {{$key->writers['phone']}}
                            </address>
                        </div>
                        <div class="pull-right m-t-30">
                            <p><strong>Order Date: </strong> {{$key->created_at}} </p>
                            <p class="m-t-10"><strong>Invoice Status: </strong> <span class="label label-pink">{{$key->status}}</span></p>
                            <p class="m-t-10"><strong>Invoice Number: </strong> {{$key->invoice_no}}</p>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <div class="m-h-50"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table m-t-30">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Academic Level</th>
                                        <th>Page(s)</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $order = json_decode($key->orders);
                                        foreach ($order as $val) {
                                            $o = App\Order::where('id', $val)->get(); ?>
                                            
                                            @foreach($o as $value)
                                            <tr>
                                                <td>{{ $value->id }}</td>
                                                <td>{{ $value->title }}</td>
                                                <td>{{ $value->academic_level }}</td>
                                                <td>{{ $value->words }}</td>
                                                <td>{{ $value->status }}</td>
                                                <td>{{ $value->writer_pay }}</td>
                                            </tr>
                                            @endforeach
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="clearfix m-t-40">
                            <h5 class="small text-inverse font-600">PAYMENT</h5>

                            <small>
                                This invoice has already been paid for.
                            </small>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-2">
                        <p class="text-right"><b>Sub-total:</b> {{ $key->total }}</p>
                        <!-- <p class="text-right">Discout: 12.9%</p>
                        <p class="text-right">VAT: 12.9%</p> -->
                        <hr>
                        <h3 class="text-right">USD {{ $key->total }}</h3>
                    </div>
                </div>
                <hr>
                <div class="hidden-print">
                    <div class="pull-right">
                        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                        <!-- <button class="btn btn-primary waves-effect waves-light"onclick="add_invoice()">Approve</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 @endforeach
@endsection
