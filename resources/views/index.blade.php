<!DOCTYPE html>
<html lang="en">
<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Writers, Writing, academic writing, referencing styles, creative writing" />
    <meta name="description" content="A Web Portal for writers with direct clients and who manage their own writers." />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- title  -->
    <title>Writers Web</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />
    <!-- plugins -->
    <!-- <link rel="stylesheet" href="{{ asset('landing/plugins.css') }}" /> -->
    <link href="{{ asset('landing/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/fontawesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/nav-menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/mailform.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <!-- search css -->
    <link rel="stylesheet" href="{{ asset('landing/search.css') }}" />

    <!-- switcher css -->
    <link href="{{ asset('landing/switcher.css') }}" rel="stylesheet">

    <!-- core style css -->
    <link href="{{ asset('landing/styles.css') }}" rel="stylesheet" id="colors" />
<style>body{background-image: url("../img/pattren5.jpg");}</style>
</head>

<body>

    <!-- start page loading -->
    <div id="preloader">
        <div class="row loader">
            <div class="loader-icon"></div>
        </div>
    </div>
    <!-- end page loading -->

    <!-- start main-wrapper section -->
    <div class="main-wrapper wrapper-boxed">

        <!-- Start Header -->
        <header class="header onepage-header" data-scroll-index="0">

            <div class="container">

                <div class="menu_area alt-font">

                    <!-- Start Navbar -->
                    <nav class="navbar navbar-expand-lg no-padding">

                        <div class="container sm-position-relative">

                            <div class="navbar-header navbar-header-custom">
                                <!-- start logo -->
                                <a href="/" class="navbar-brand white-logo"><img id="logo" src="{{ asset('img/logo.png') }}" alt="logo"></a>
                                <!-- end logo -->
                            </div>

                            <div class="navbar-toggler"></div>

                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="javascript:void(0);" data-scroll-nav="0">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="1">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="2">Services</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="3">Pricing</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-scroll-nav="6">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <!-- <div class="margin-20px-left sm-display-none"> -->
                                        <a class="nav-link butn" href="/login">Login</a>
                                    <!-- </div> -->
                                </li>
                                <li class="nav-item">
                                    <!-- <div class="margin-20px-left sm-display-none"> -->
                                        <a class="nav-link butn" href="/order-now">Order Now</a>
                                    <!-- </div> -->
                                </li>
                            </ul>


                        </div>

                    </nav>
                    <!-- End Navbar  -->
                </div>

            </div>

        </header>
        <!-- End Header -->

        <!-- start main banner area -->
        <div class="banner-creative bg-img cover-background theme-overlay" data-scroll-index="0" data-overlay-dark="9" data-background="{{ asset('img/content-06.png')}}">

            <div class="container">

                <div class="row">

                    <!-- start left banner text -->
                    <div class="col-lg-6 col-md-12 sm-margin-50px-bottom xs-margin-20px-bottom">
                        <div class="header-text sm-width-75 xs-width-100">

                            <h1 class="line-height-55 md-line-height-50 xs-line-height-40 xs-font-size28 wow fadeInUp text-white" data-wow-delay=".1s">Our Web Portal</h1>
                            <blockquote>
                                <p class="text-white font-size16 line-height-28 xs-font-size14 xs-line-height-26 margin-30px-bottom sm-margin-20px-bottom width-80 xs-width-90 wow fadeInUp" data-wow-delay=".2s">
                                    Our portal majorly is to enable freelancers have their own Orders/Clients management system to help them in time management and help them make a little bit more money as compared to other online jobs system. <br>
                                    When one uses our portal they are guaranteed to have ample time since they are the admins of the portal and for the users it's easy to use and very efficient. <br>
                                    It also ensures the client has easy tracking of progress for jobs given. <br>
                                    Keeps track of financial history hence no misunderstandings. <br>
                                    And many more...
                                </p>
                            </blockquote>
                            <div class="wow fadeInUp story-video" data-wow-delay=".4s">
                                <a href="tel:+254715274848" class="butn white margin-10px-right vertical-align-middle"><i class="fa fa-phone-square"></i> Get Started</a>
                                
                            </div>

                        </div>
                    </div>
                    <!-- end banner text -->

                    <!-- start right image banner -->
                    <div class="col-lg-6 col-md-12 sm-text-center">
                        <div class="banner-mg">
                            <img style="box-shadow:4px 4px #aaaaaa; border-radius:10px; border:10px solid #f7f7f7;" src="{{ asset('img/slide.png') }}" class="img-fluid float-right width-100" alt="">
                        </div>
                    </div>
                    <!-- end right image banner -->

                </div>

            </div>

        </div>
        <!-- end main banner area -->

        <!-- start shape area -->
        <div class="header-shape xs-display-none">
            <img src="{{ asset('img/header-bg03.png') }}" class="img-fluid width-100" alt="">
        </div>
        <!-- end shape area -->

        <!-- start service section -->
        <section data-scroll-index="1">
            <div class="container">
                <div class="text-center section-heading">
                    <h2>We Provide Awesome Modules</h2>
                    <p class="width-55 sm-width-75 xs-width-95">All modules are equiped with the required functionalities to facilitate an order to be done from inception to the very end.</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-12 sm-margin-20px-bottom">
                        <div class="services-block bg-light-gray padding-45px-tb padding-25px-lr sm-padding-35px-tb sm-padding-20px-lr xs-padding-30px-tb xs-padding-15px-lr last-paragraph-no-margin wow fadeInUp" data-wow-delay=".2s">
                            <div class="title-box margin-25px-bottom sm-margin-15px-bottom">
                                <i class="fa fa-user-plus text-theme-color"></i>
                                <div class="box-circle-large"></div>
                                <div class="box-circle-small"></div>
                            </div>
                            <h3 class="margin-10px-bottom font-size22 md-font-size20 xs-font-size18">Admin Module</h3>
                            <p class="font-size16 line-height-28">The admin module allows one to be able to manage orders and their writers as well.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 sm-margin-20px-bottom">
                        <div class="services-block bg-light-gray padding-45px-tb padding-25px-lr sm-padding-35px-tb sm-padding-20px-lr xs-padding-30px-tb xs-padding-15px-lr last-paragraph-no-margin wow fadeInUp" data-wow-delay=".4s">
                            <div class="title-box margin-25px-bottom sm-margin-15px-bottom">
                                <i class="fa fa-users text-theme-color"></i>
                                <div class="box-circle-large"></div>
                                <div class="box-circle-small"></div>
                            </div>
                            <h3 class="margin-10px-bottom font-size22 md-font-size20 xs-font-size18">Client Module</h3>
                            <p class="font-size16 line-height-28 sm-font-size14 sm-line-height-24">The client module allows a client to post a job and check its progress also one can chat with the admin directly.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="services-block bg-light-gray padding-45px-tb padding-25px-lr sm-padding-35px-tb sm-padding-20px-lr xs-padding-30px-tb xs-padding-15px-lr last-paragraph-no-margin wow fadeInUp" data-wow-delay=".6s">
                            <div class="title-box margin-25px-bottom sm-margin-15px-bottom">
                                <i class="fa fa-pencil-square text-theme-color"></i>
                                <div class="box-circle-large"></div>
                                <div class="box-circle-small"></div>
                            </div>
                            <h3 class="margin-15px-bottom font-size22 md-font-size20 xs-font-size18">Writers Module</h3>
                            <p class="font-size16 line-height-28 sm-font-size14 sm-line-height-24">One can view available orders and can be assigned jobs as well as keep track of financial history.</p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- end service section -->

        <!-- start business service section -->
        <section class="bg-light-gray" data-scroll-index="2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 sm-text-center sm-margin-0px-bottom">
                        <img src="{{ asset('img/content-06.png')}}" alt="" />
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="padding-30px-left sm-no-padding padding-0px-tb md-padding-0px-tb">
                            
                            <h4 class="sm-margin-lr-auto margin-20px-bottom xs-width-100">Statistics & Analytics</h4>
                            <p class="margin-10px-bottom">We provide ample statistics for analysis purposes so that one can know how their accounts are doing hence make informed decisions when they have to <br> i.e. whether to add new writers due to a high number of clients and available orders.</p>
                            <p class="margin-10px-bottom">For easier communication there's a chat module that allows clients to chat directly with the admin and vice versa too.</p>
                            <p class="margin-10px-bottom">Also for easier financial transactions, the portal has been integrated with PayPal.</p>
                            <a href="tel:+254715274848" class="butn"><i class="fa fa-phone-square"></i> Learn more</a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- end business section -->

        <!-- start business service section -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 order-2 order-lg-1">
                        <div class="padding-30px-right sm-no-padding padding-0px-tb md-padding-30px-tb">
                            

                            <h4>Summary</h4>
                            <p>The best way to know about the portal is to interact with it in order to have a fast hand experience.</p>

                            <ul class="list-style margin-30px-bottom">
                                <li>Simple and easy to understand.</li>
                                <li>Saves time in terms of assigning and communications.</li>
                                <li>Easy Upload and Download of Files.</li>
                                <li>Email notifications for all orders.</li>
                            </ul>

                            <a href="tel:+254715274848" class="butn"><i class="fa fa-phone-square"></i> Learn more</a>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-12 sm-text-center sm-margin-20px-bottom order-1 order-lg-2">
                        <img src="{{ asset('img/content-07.png') }}" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <!-- end business section -->

        <!-- start counter section -->
        <section class="bg-theme">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-lg-3 col-md-6 sm-margin-30px-bottom">
                        <div class="counter-box">
                            <span class="icon margin-10px-bottom display-block text-white font-size36 xs-font-size30 xs-text-center"><i class="fa fa-user"></i></span>
                            <h4 class="countup text-white display-block xs-text-center">1826</h4>
                            <div class="separator-line-horrizontal-medium-light3 bg-white margin-15px-tb xs-margin-10px-tb opacity5 center-col"></div>
                            <p class="font-size18 sm-font-size16 font-weight-600 text-white no-margin text-center">Satisfied Visitors</p>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-6 sm-margin-30px-bottom">
                        <div class="counter-box">
                            <span class="icon margin-10px-bottom display-block text-white font-size36 xs-font-size30 xs-text-center"><i class="fa fa-smile-o"></i></span>
                            <h4 class="countup text-white display-block xs-text-center">875</h4>
                            <div class="separator-line-horrizontal-medium-light3 bg-white margin-15px-tb xs-margin-10px-tb opacity5 center-col"></div>
                            <p class="font-size18 sm-font-size16 font-weight-600 text-white no-margin text-center">Happy Clients</p>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-6">
                        <div class="counter-box">
                            <span class="icon margin-10px-bottom display-block text-white font-size36 xs-font-size30 xs-text-center"><i class="fa fa-trophy"></i></span>
                            <h4 class="countup text-white display-block xs-text-center">1412</h4>
                            <div class="separator-line-horrizontal-medium-light3 bg-white margin-15px-tb xs-margin-10px-tb opacity5 center-col"></div>
                            <p class="font-size18 sm-font-size16 font-weight-600 text-white no-margin text-center">Awards Wining</p>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-6">
                        <div class="counter-box">
                            <span class="icon margin-10px-bottom display-block text-white font-size36 xs-font-size30 xs-text-center"><i class="fa fa-life-ring"></i></span>
                            <h4 class="countup text-white display-block xs-text-center">100</h4>
                            <div class="separator-line-horrizontal-medium-light3 bg-white margin-15px-tb xs-margin-10px-tb opacity5 center-col"></div>
                            <p class="font-size18 sm-font-size16 font-weight-600 text-white no-margin text-center">Consultation</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end counter section -->

        <!-- start pricing table -->
        <!-- <section class="price bg-light-gray" data-scroll-index="3">
            <div class="container">
                <div class="section-heading">
                    <h3>Pricing</h3>
                </div>
                <div class="row"> -->

                    <!-- start table -->
                    <!-- <div class="col-lg-4 sm-margin-20px-bottom"> -->
                        <!-- <div class="item text-center">
                            <div class="type">
                                <h4 class="font-size22 sm-font-size20 xs-font-size18">Basic</h4>
                            </div>
                            <div class="value">
                                <h3>10<span>$</span></h3>
                                <span class="per">Per Month</span>
                            </div>
                            <div class="features">
                                <ul>
                                    <li>24/7 Tech Support</li>
                                    <li>Advanced Options</li>
                                    <li>1GB Storage</li>
                                    <li>6GB Bandwidth</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>
                            <div class="order">
                                <a href="javascript:void(0);" class="butn">Purchase Now</a>
                            </div>
                        </div> -->
                    <!-- </div> -->
                    <!-- end table -->

                    <!-- start table -->
                    <!-- <div class="col-lg-4 sm-margin-20px-bottom">
                        <div class="item text-center active">
                            <div class="type">
                                <h4 class="font-size22 sm-font-size20 xs-font-size18">Standard</h4>
                            </div>
                            <div class="value">
                                <h3>500<span><i class="fa fa-dollar"></i></span></h3>
                            </div>
                            <div class="features">
                                <ul>
                                    <li>24/7 Tech Support</li>
                                    <li>Admin Module</li>
                                    <li>Writers Module</li>
                                    <li>Clients Module</li>
                                    <li>Notifications Module</li>
                                    <li>Chat Module</li>
                                    <li>PayPal/Financial Module</li>
                                </ul>
                            </div>
                            <div class="order">
                                <a href="tel:+254715274848" class="butn"><i class="fa fa-phone-square"></i> Contact Now</a>
                            </div>
                        </div>
                    </div> -->
                    <!-- end table -->

                    <!-- start table -->
                    <!-- <div class="col-lg-4">
                        <div class="item text-center">
                            <div class="type">
                                <h4 class="font-size22 sm-font-size20 xs-font-size18">Premium</h4>
                            </div>
                            <div class="value">
                                <h3>99<span>$</span></h3>
                                <span class="per">Per Month</span>
                            </div>
                            <div class="features">
                                <ul>
                                    <li>24/7 Tech Support</li>
                                    <li>Advanced Options</li>
                                    <li>6GB Storage</li>
                                    <li>12GB Bandwidth</li>
                                    <li>Unlimited Support</li>
                                </ul>
                            </div>
                            <div class="order">
                                <a href="javascript:void(0);" class="butn">Purchase Now</a>
                            </div>
                        </div>
                    </div> -->
                    <!-- end table -->

                <!-- </div>
            </div>
        </section> -->
        <!-- end pricing table -->

        <!-- start innovate business section -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-12 sm-text-center sm-margin-0px-bottom">
                        <img src="{{ asset('img/content-03.png') }}" alt="" />
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <div class="padding-0px-left sm-no-padding">
                            <h4 class="sm-margin-lr-auto sm-text-center xs-width-100 xs-margin-30px-bottom">Frequently Asked Questions</h4>
                            <div id="accordion" class="accordion-style">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    How can I purchase this portal ?
                                    </button>
                                    </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            Kindly contact this number <a href="tel:+254715274848"><b>+254715274848</b></a> for more details and on how to go around purchasing the portal.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Is it a one time payment or subscription ?
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            It is a one time payment but tech support is 24/7 for a whole year.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        If I want something to be added will it cost me ?
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            Yes, for any additional requirements one will be charged depending on complexity.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Are notifications via text messages(SMS) ?
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            Email only but if you want via text we can add it for you at an extra cost as described above since its a new feature.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- end innovate business section -->

        <!-- start contact form -->
        <section data-scroll-index="6" class="bg-light-gray">
            <div class="container">
                <div class="row">
                    <div class="col-12 wow fadeIn">
                         
                        <div class="section-heading">
                            <h3>Get in Touch</h3>
                            <p class="width-55 sm-width-75 xs-width-95">We are available 24/7 by e-mail and phone. You can also ask a question about our services through our contact form that we regularly provide.</p>
                        </div>                            
                            
                            <form class="mailform off2" action="/contact-form" method="post">
                            {{csrf_field()}}
                              <div class="row">
                                <div class="col-md-4">
                                  <input type="text" name="name" placeholder="Your Name:" data-constraints="@LettersOnly @NotEmpty">
                                </div>
                                <div class="col-md-4">
                                  <input type="text" name="phone" placeholder="Telephone:" data-constraints="@Phone">
                                </div>
                                <div class="col-md-4">
                                  <input type="text" name="email" placeholder="Email:" data-constraints="@Email @NotEmpty">
                                </div>
                                <div class="col-md-12">
                                  <textarea name="message" rows="5" placeholder="Message:" data-constraints="@NotEmpty"></textarea>
                                </div>
                                <div class="mfControls col-md-12">
                                  <button type="submit" class="butn">Submit comment</button>
                                </div>
                              </div>
                            </form>
                     
                    </div>
                </div>
            </div>
        </section>        
        <!-- end contact form -->

        <!-- start footer section -->
        <!-- <footer> -->
            <div class="footer-bar">
                <div class="container">
                    <p class="xs-font-size13">&copy; 2019 : <a href="/">Writers Web</a></p>
                </div>
            </div>
        <!-- </footer> -->
        <!-- end footer section -->

    </div>
    <!-- end main-wrapper section -->

    <!-- start scroll to top -->
    <a href="javascript:void(0)" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    <!-- end scroll to top -->

    <!-- all js include start -->

    <!-- Java script -->
    <script src="{{ asset('landing/core.min.js') }}"></script>

    <!-- serch -->
    <script src="{{ asset('landing/search.js') }}"></script>
    
    <!-- custom scripts -->
    <script src="{{ asset('landing/main.js') }}"></script>

    <!-- contact form scripts -->
    <script src="{{ asset('landing/jquery.form.min.js') }}"></script>
    <script src="{{ asset('landing/jquery.rd-mailform.min.c.js') }}"></script>

    <!-- all js include end -->

</body>
</html>