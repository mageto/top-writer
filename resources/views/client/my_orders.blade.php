@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Subject</th>
                <th>Topic</th>
                <th>Academic Level</th>
                <th>Date Posted</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($orders as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->subjects->subject}}</td>
                <td>{{$value->topic}}</td>
                <td>{{$value->academic_level}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->status}}</td>
                <td>
                    <a class="btn btn-sm btn-primary" href="/my-order/{{$value->id}}">View</a>
                    <?php
                    if ($value->status == 'Open') { ?>
                        <a class="btn btn-sm btn-warning" onclick="cancel_order({{$value->id}})">Cancel</a>
                        <a class="btn btn-sm btn-danger" onclick="delete_order({{$value->id}})">Delete</a>
                    <?php } ?>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script type="text/javascript">
    
    function delete_order(id){
        swal({
            title: "Are you sure,",
            text: "You want to delete this order!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            },
            function(){
                // ajax delete data to database
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : "delete-order/"+id,
                    type: "DELETE",
                    dataType: "JSON",
                    success: function(data)
                    {
                        swal({   title: "Deleted",   
                            text: "Order has been Deleted.",   
                            timer: 1000,
                            type: "success",   
                            showConfirmButton: false 
                        });
                        location.reload();
                    },
                });
            });
    }
    function cancel_order(id){
        swal({
            title: "Are you sure,",
            text: "You want to Cancel this order!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Cancel it!",
            closeOnConfirm: false
            },
            function(){
                // ajax cancel data to database
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : "cancel-order/"+id,
                    type: "PUT",
                    dataType: "JSON",
                    success: function(data)
                    {
                        swal({   title: "Canceled",   
                            text: "Order has been canceled.",   
                            timer: 1000,
                            type: "success",   
                            showConfirmButton: false 
                        });
                        location.reload();
                    },
                });
            });
    }
</script>

@endsection