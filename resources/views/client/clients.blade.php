@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($clients as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->name}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->phone}}</td>
                <td>
                    <a class="btn btn-sm btn-primary" href="/client/{{$value->id}}">View</a>
                    <a class="btn btn-sm btn-danger" onclick="delete_client({{$value->id}})">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>

    
    function delete_client(id){
        swal({
            title: "Are you sure,",
            text: "You want to delete this Client!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            },
            function(){
                // ajax delete data to database
                    $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : "delete-client/"+id,
                    type: "DELETE",
                    dataType: "JSON",
                    success: function(data)
                    {
                        swal({   title: "Deleted",   
                            text: "Client has been Deleted.",   
                            timer: 1000,
                            type: "success",   
                            showConfirmButton: false 
                        });
                        location.reload();
                    },
                });
            });
    }
</script>
@endsection