@extends('layouts.app')

@section('content')
<div class="row">
    @foreach($orders as $value)
    <div class="col-lg-8">
        <div class="card-box project-box">
            <div class="label label-success">{{$value->status}}</div>
            <h4 class="m-t-0 m-b-5"><a href="" class="text-inverse">{{$value->topic}}</a></h4>

            <p class="text-success text-uppercase m-b-20 font-13">{{$value->subjects->subject}}</p>
            <p class="text-muted font-13">{{$value->instructions}}
            </p>

            <ul class="list-inline">
                <li>
                    <b><p>Academic Level</p></b>
                    <p class="text-muted">{{$value->academic_level}}</p>
                </li>
                <li>
                    <b><p>Sources</p></b>
                    <p class="text-muted">{{$value->sources}}</p>
                </li>
                <li>
                    <b><p>Words</p></b>
                    <p class="text-muted">{{$value->words * 275}}</p>
                </li>
                <li>
                    <b><p>Urgency</p></b>
                    <p class="text-muted">{{$value->urgency}}</p>
                </li>
                <li>
                    <b><p>Referencing Style</p></b>
                    <p class="text-muted">{{$value->referencing_style}}</p>
                </li>
                @if( Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'client' )
                <li>
                    <b><p>Amount</p></b>
                    <p class="text-muted">{{$value->amount}}</p>
                </li>
                <li>
                    <b><p>Deadline</p></b>
                    <p id="deadline1"></p>
                </li>
                @endif
                @if( Auth::user()->user_type == 'writer' )
                <li>
                    <b><p>Deadline</p></b>
                    <p id="deadline2"></p>
                </li>
                @endif
            </ul>
            <!-- Display the countdown timer in an element -->
            <input type="hidden" id="deadline" value="{{strftime($value->deadline)}}">
            <input type="hidden" id="deadline_writer" value="{{strftime($value->deadline_writers)}}">

<script>
    var countDownDate = new Date(document.getElementById("deadline").value).getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("deadline1").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        if (distance < 0) {
                clearInterval(x);
                document.getElementById("deadline1").innerHTML = "Deadline Passed";
            }
    }, 1000);


    var countDownDatewriter = new Date(document.getElementById("deadline_writer").value).getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDatewriter - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        document.getElementById("deadline2").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        if (distance < 0) {
                clearInterval(x);
                document.getElementById("deadline2").innerHTML = "Deadline Passed";
            }
    }, 1000);

</script>
            <ul class="list-inline task-dates m-b-0 m-t-20">
                <li>
                    <h5 class="font-600 m-b-5">Start Date</h5>
                    <p> {{$value->created_at}}</p>
                </li>
                @if( Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'client' )
                <li>
                    <h5 class="font-600 m-b-5">Due Date</h5>
                    <p> {{$value->deadline}}</p>
                </li>
                @endif
                @if( Auth::user()->user_type == 'writer' )
                <li>
                    <h5 class="font-600 m-b-5">Due Date</h5>
                    <p> {{$value->deadline_writers}}</p>
                </li>
                @endif
            </ul>
            
            <div class=" task-detail attached-files m-t-10">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client Attached Files</h3>
                    </div>
                    <div class="panel-body">
                        <div class="files-list">
                            <?php 
                                $files = App\OrderFile::where('order_id', $value->id)->get();
                            ?>
                            <ol>
                                <blockquote>
                                    @foreach($files as $file)
                                        <li style="margin-left:20px"><a href="{{asset('order-files/')}}/{{$file->file_name}}" file="{{$file->file_name}}" download>{{$file->file_name}}</a></li>
                                    @endforeach
                                </blockquote>
                            </ol>
                        </div>
                        @if( Auth::user()->user_type == 'client' )
                        <div class="file-box m-l-15">
                            <form action="/client-files" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="hidden" name="order_id" id="order_id" value="{{ $id }}">
                                    <div class="col-sm-9">
                                        <button id="ADDFILE" class=" m-b-5 btn btn-sm btn-danger fullwidth"> Add/upload file(s)</button>
                                        <div style="width: max-content;" id="uploadFileContainer"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-rght m-t-10">
                                        <button type="submit" class="btn btn-success btn-sm waves-effect waves-light">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="panel panel-color panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Writer Attached Files</h3>
                    </div>
                    <div class="panel-body">
                        <div class="files-list">
                            <?php 
                                $files = App\WriterOrderFile::where('order_id', $id)->get();
                            ?>
                            
                            @if( Auth::user()->user_type == 'admin' || $value->status == 'Complete' )
                            <ol>
                                <blockquote>
                                    @foreach($files as $file)
                                        <li style="margin-left:20px"><a href="{{asset('writer-order-files/')}}/{{$file->file_name}}" file="{{$file->file_name}}" download>{{$file->file_name}}</a></li>
                                    @endforeach
                                </blockquote>
                            </ol>
                            @endif
                        </div>
                        
                        @if( Auth::user()->user_type == 'writer' )
                        <div class="file-box m-l-15">
                            <form action="/writer-files" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="hidden" name="order_id" id="order_id" value="{{ $id }}">
                                    <div class="col-sm-9">
                                        <button id="ADDFILE" class=" m-b-5 btn btn-sm btn-danger fullwidth"> Add/upload file(s)</button>
                                        <div style="width: max-content;" id="uploadFileContainer"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-rght m-t-10">
                                        <button type="submit" class="btn btn-success btn-sm waves-effect waves-light">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($value->status == 'Open')
            <p class="font-600 m-b-5">Progress <span class="text-danger pull-right">10%</span></p>
            <div class="progress progress-bar-danger-alt progress-sm m-b-5">
                <div class="progress-bar progress-bar-danger progress-animated wow animated animated"
                        role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"
                        style="width: 10%; visibility: visible; animation-name: animationProgress;">
                </div><!-- /.progress-bar .progress-bar-danger -->
            </div><!-- /.progress .no-rounded -->
            @elseif($value->status == 'Assigned')
            <p class="font-600 m-b-5">Progress <span class="text-primary pull-right">50%</span></p>
            <div class="progress progress-bar-primary-alt progress-sm m-b-5">
                <div class="progress-bar progress-bar-primary progress-animated wow animated animated"
                        role="progressbar" aria-valuenow="50" aria-valuemin="30" aria-valuemax="100"
                        style="width: 50%; visibility: visible; animation-name: animationProgress;">
                </div><!-- /.progress-bar .progress-bar-danger -->
            </div><!-- /.progress .no-rounded -->
            @elseif($value->status == 'Edit')
            <p class="font-600 m-b-5">Progress <span class="text-primary pull-right">70%</span></p>
            <div class="progress progress-bar-primary-alt progress-sm m-b-5">
                <div class="progress-bar progress-bar-primary progress-animated wow animated animated"
                        role="progressbar" aria-valuenow="70" aria-valuemin="50" aria-valuemax="100"
                        style="width: 70%; visibility: visible; animation-name: animationProgress;">
                </div><!-- /.progress-bar .progress-bar-danger -->
            </div><!-- /.progress .no-rounded -->
            @else
            <p class="font-600 m-b-5">Progress <span class="text-success pull-right">100%</span></p>
            <div class="progress progress-bar-success-alt progress-sm m-b-5">
                <div class="progress-bar progress-bar-success progress-animated wow animated animated"
                        role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                        style="width: 100%; visibility: visible; animation-name: animationProgress;">
                </div><!-- /.progress-bar .progress-bar-danger -->
            </div><!-- /.progress .no-rounded -->
            @endif
        </div>
    </div><!-- end col-->
    @endforeach
    <div class="col-lg-4">
        <div class="card-box">
            @if( Auth::user()->user_type == 'admin' )
            <h4>Assign Writer</h4>
            <form action="/assign-order" method="post">
                {{ csrf_field() }}
                @foreach($orders as $value)
                    <input type="hidden" value="{{ $value->id }}" name="order_id">
                @endforeach
                <div class="form-group">
                    <label for="writer_id">Choose Writer</label>
                    <select class="form-control select2" name="writer_id">
                        <option>Choose Writer</option>
                        @foreach($writers as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="writer_pay">Writer Pay</label>
                    <input type="number" class="form-control" name="writer_pay">
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-success btn-block waves-effect w-md waves-light m-t-5" type="submit" value="">Assign</button>
                </div>
            </form>
            <hr>
            <h4>Mark Completed</h4>
            <p>
                <blockquote>
                    Mark this order as completed if you have uploaded all the required files and ensured the order is done.
                </blockquote>
            </p>
            <form action="/completed-order" method="post">
                {{ csrf_field() }}
                @foreach($orders as $value)
                    <input type="hidden" value="{{ $value->id }}" name="order_id">
                @endforeach
                <div class="">
                    <button class="btn btn-sm btn-primary btn-block waves-effect w-md waves-light m-t-5" type="submit" value="">Mark Completed</button>
                </div>
            </form>
            @endif
            @if( Auth::user()->user_type == 'writer' )
            <h4>Deliver</h4>
            <p>
                <blockquote>
                    Deliver this order if you have uploaded all the required files and ensured the order is done.
                </blockquote>
            </p>
            <form action="/completed-order-writer" method="post">
                {{ csrf_field() }}
                @foreach($orders as $value)
                    <input type="hidden" value="{{ $value->id }}" name="order_id">
                @endforeach
                <div class="">
                    <button class="btn btn-sm btn-primary btn-block waves-effect w-md waves-light m-t-5" type="submit" value="">Deliver</button>
                </div>
            </form>
            @endif
            @if( Auth::user()->user_type == 'client' )
            <h4>Mark for Revision</h4>
            <p>
                <blockquote>
                    If the order is not as requested, submit it for revision.
                </blockquote>
            </p>
            <form action="/revision" method="post">
                {{ csrf_field() }}
                @foreach($orders as $value)
                    <input type="hidden" value="{{ $value->id }}" name="order_id">
                @endforeach
                <div class="">
                    <button class="btn btn-sm btn-primary btn-block waves-effect w-md waves-light m-t-5" type="submit" value="">Submit for Revision</button>
                </div>
            </form>
            @endif
        </div>
        <div class="card-box">
        <h3>Rate This Order</h3>
            <?php
                if ($rate > 0) {
                    for ($i=0; $i < $rate; $i++) { ?>
                        <i onclick="rate(<?php echo $i+1 . ',' . $id; ?>)" class="fa fa-star"></i>
                <?php } for ($i=0; $i < (5 - $rate); $i++) { ?>
                        <i onclick="rate(<?php echo $i+1+$rate . ',' . $id; ?>)" class="fa fa-star-o"></i>
                <?php } } else { for ($i=1; $i < 6; $i++) { ?>
                    <i onclick="rate(<?php echo $i . ',' . $id; ?>)" class="fa fa-star-o"></i>
            <?php }} ?>
        <script>
            function rate(i, id){
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    }
                });

                $.ajax({
                    url: '/rate-order', // point to server-side PHP script
                    data: { id : id, rate : i } ,
                    type: 'POST',
                    success: function(data) {
                            location.reload();
                    },
                    error: function (error, data) {
                        console.log('Error:', data);
                    }
                });
            }
        </script>

        </div>
    </div>
</div>
<!-- end row -->

<script type="text/javascript">
  jQuery(document).ready(function($){

    $(document).on('click', 'button#ADDFILE', function(event) {
      event.preventDefault();
      $("div#submit").css("display", "block")
      addFileInput();
    });

    function addFileInput() {
      var html ='';
      html +='<div class="alert alert-info">';
      html +='<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;</button>';
      html +='<strong> Upload file </strong>';
      html +='<input type="file" name="multipleFiles[]">';
      html +='<input type="hidden" name="upload_type" value="material">';
      html +='</div>';

      $("div#uploadFileContainer").append(html);
    }

  });

</script>

@endsection