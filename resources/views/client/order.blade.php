@extends('layouts.app')

@section('content')
    
        <div class="row">
            <div class="col-lg-12">
                <!-- <div class="card-box">  -->
              <div class="col-md-12 card-box">
                  <div class="main border-right">
                    <h3>New order</h3>
                    @if (session('error') || session('success'))
                    <p class="{{ session('error') ? 'error':'success' }}">
                      {{ session('error') ?? session('success') }}
                    </p>
                    @endif
                    <hr/>
                    <div class="col-md-7">
                      <form class="form-horizontal" action="/add-order" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="title">Title</label>
                          <div class="col-sm-9">
                            <input type="text" name="topic" class="form-control form" id="title" placeholder="Enter topic" required>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="subject">Subject (category)</label>
                          <div class="col-sm-9 select">          
                            <select name='subject' id='subject' class="form-control" required >
                              <option value=""> Select Subject </option>
                              <?php foreach ($subject as $key) { ?>
                                <option value={{ $key->id }}>{{ $key->subject }}</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="academiclevel">Academic level</label>
                          <div class="col-sm-9 select">          
                            <select id='academic_level' name='academic_level' class="form-control"   required >
                              <option value=""> Select Academic Level </option>
                              <?php foreach ($academics as $key) { ?>
                                <option value={{ $key->academic_level }}>{{ $key->name }}</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="words">Word/Pages</label>
                          <div class="col-sm-9 select">          
                            <select id="words" name='words'  class="form-control"   required >
                              <?php for ($x = 1; $x <= 100; $x++) {  ?>
                                <option value="<?php echo $x; ?>"> <?php echo $x; ?> Pages || Aprox <?php $words=$x*275; echo $words; ?> Words </option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-3 text-right" for="urgency">Urgency</label>
                          <div class="col-sm-9 select">          
                              <select  id='urgency' name='urgency' class="form-control" required>
                                <?php foreach ($urgency as $key) { ?>
                                  <option style="text-transform:capitalize;" value="{{ $key->deadline }}">{{ $key->deadline }}</option>
                                <?php } ?>
                              </select>
                        </div>
                    </div>
                      <div class="form-group">
                        <div class="amount">
                          <label class="control-label col-sm-4 text-right" for="">Amount </label>
                          <div class="col-sm-2 select">
                            <select  id='currency' name='currency' class="form-control " >
                                    <option value="1">USD</option>
                                    <option value="1.1">EUR</option>
                                    <option value="1.3">AUD</option>
                                    <option value="0.69">GBP</option>
                                  </select>

                          </div>
                          <div class="col-sm-5">
                            <input type="input" id="amount" class="form-control" name="amount" value="$" placeholder="10" readonly/>
                            <input type="hidden" id="usdamount" class="form-control" name="usdamount" value="$" placeholder="10" readonly/>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="pwd">Number of Sources</label>
                        <div class="col-sm-9 select">          
                          <select id='sources' name='sources'  class="form-control"  >
                            <option value="1"> Select number of Sources </option>
                            <?php for ($x = 1; $x <= 100; $x++) {  ?>
                            <option value="<?php echo $x; ?>"> <?php echo $x; ?>  </option>
                            <?php } ?> 
                          </select>
                        </div>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="pwd">Format style</label>
                        <div class="col-sm-9 select">          
                          <select name='referencing_style' id='referencing_style' class="form-control select2"  >
                            <option> Select Referencing Style </option>
                            <option value="MLA">MLA</option>
                            <option value="HAVARD">HAVARD</option>
                            <option value="Chicago">Chicago</option>
                            <option value="APA">APA</option>
                            <option value="Turabian">Turabian</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="instructions">Instructions</label>
                        <div class="col-sm-9">
                          <textarea class="ckeditor form-control" id='editor'  name="instructions"></textarea>
                        </div>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <label class="control-label col-sm-3 text-right" for="email">Additional materials</label>
                        <div class="col-sm-9">
                          <button id="ADDFILE" class="btn btn-danger fullwidth"> Add/upload file(s)</button>
                          <div id="uploadFileContainer"></div>
                        </div>
                      </div>
                      <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-9">
                        <a 
                          href="#custom-modal" 
                          class="btn btn-primary btn-block waves-effect waves-light m-r-5 m-b-10" 
                          data-animation="blur" 
                          data-plugin="custommodal"
                          data-overlaySpeed="100"
                          onclick="confirm_order()" 
                          data-overlayColor="#36404a">Submit
                        </a>
                        <button style="display:none;" type="submit" id="submit" name="submit">Proceed to Payment</button>
                          <!-- Modal -->
                          <div id="custom-modal" class="modal-demo">
                              <button type="button" class="close" onclick="Custombox.close();">
                                  <span>&times;</span><span class="sr-only">Close</span>
                              </button>
                              <h4 class="custom-modal-title">Confirm Order</h4>
                              <div class="custom-modal-text">
                                  <div class="card-box">
                                      <table style="width:500px; text-align:left;" class="table m-0">
                                          <thead>
                                              <tr>
                                                  <th colspan="2" >Details</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr>
                                                  <td>Order Topic</td>
                                                  <td id=""><label style="text-transform:capitalize;" id="topics"></label></td>
                                              </tr>
                                              <tr>
                                                  <td>Subject</td>
                                                  <td id=""> <label style="text-transform:capitalize;" id="subjects"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Academic Level</td>
                                                  <td id=""> <label style="text-transform:capitalize;" id="academic_levels"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Words/Pages</td>
                                                  <td id=""> <label id="word"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Urgency</td>
                                                  <td id=""> <label id="urgent"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Referencing Style</td>
                                                  <td id=""> <label id="referencing_styles"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Sources</td>
                                                  <td id=""> <label id="source"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Instructions</td>
                                                  <td id=""> <label id="instruction"></label> </td>
                                              </tr>
                                              <tr> 
                                                  <td><strong>Amount</strong> </td>
                                                  <td id=""><strong> <u><label id="amounts"></label></u>  </strong></td>
                                              </tr>
                                              <tr>
                                                <td colspan="2"><button type="submit" onclick="me()" class="btn btn-info btn-block fullwidth">Proceed to Payment</button></td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                            
                          </div>
                        </div>
                      </div>
                  </form>
                  </div>
                  
                

                  <div class="col-md-5">
                    <!-- <div class="col-sm-4"> -->
                    <h3> 100% Custom in 3-steps </h3>
                    <div class="row" style="border: 1px dotted #31b0d5;">
                      <div class="col-sm-3"><h1><i class="fa fa-book fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Fill Order Details</h5><p>  Fill In the Instructions of Your Essays.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-check    fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Confirm</h5><p> Confirm Order is Correct.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-paypal fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Pay for your Order</h5><p>Pay via Paypal/Credit Card.</p></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<script>
  function me(){
    // alert('Me was clicked');
    document.getElementById('submit').click();
  }
  function confirm_order(){
    var urgency = document.getElementById('urgency').value;
    var academic_level = document.getElementById('academic_level').value;
    var words = document.getElementById('words').value;
    var topic = document.getElementById('title').value;
    var subject = document.getElementById('subject').value;
    var amount = document.getElementById('amount').value;
    var sources = document.getElementById('sources').value;
    var referencing_style = document.getElementById('referencing_style').value;
    var instructions = document.getElementById('editor').value;

    document.getElementById('urgent').innerHTML = urgency;
    document.getElementById('academic_levels').innerHTML = academic_level;
    document.getElementById('word').innerHTML = words;
    document.getElementById('topics').innerHTML = topic;
    document.getElementById('subjects').innerHTML = subject;
    document.getElementById('amounts').innerHTML = amount;
    document.getElementById('source').innerHTML = sources;
    document.getElementById('referencing_styles').innerHTML = referencing_style;
    document.getElementById('instruction').innerHTML = instructions;
    
  }
</script>

<script type="text/javascript">


  $("select[id='words'], select[id='academic_level'], select[id='subject'], input[id='discount'], select[id='currency'], select[id='urgency']").change(function(){

    $q1 = document.getElementById('urgency').value;
    $q2 = document.getElementById('academic_level').value;
    $q3 = document.getElementById('words').value;
    $q4 = document.getElementById('currency').value;
    // alert($q2);
       $.ajax({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        url : "/calculate",
        type: "get",
        data: {"urgency" : $q1, "academic_level" : $q2, "words" : $q3, "currency" : $q4},
        // dataType: "json",
        success: function(data)
        {
          var dat = JSON.parse(data);

            var h = dat[0][0].highschool;
            var m = dat[0][0].master;
            var u = dat[0][0].undergraduate;
            var d = dat[0][0].doctoral;
            var w = dat[1].words;
            var c = parseFloat(dat[2].currency);
            if (h) {
            document.getElementById('amount').value = h*w*c;
            }
            if (m) {
            document.getElementById('amount').value = m*w*c;
            }
            if (u) {
            document.getElementById('amount').value = u*w*c;
            }
            if (d) {
            document.getElementById('amount').value = d*w*c;
            }
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
            console.log(errorThrown);
           }
       });
});

</script>

<script type="text/javascript">
  jQuery(document).ready(function($){

    $(document).on('click', 'button#ADDFILE', function(event) {
      event.preventDefault();
      $("div#submit").css("display", "block")
      addFileInput();
    });

    function addFileInput() {
      var html ='';
      html +='<div class="alert alert-info">';
      html +='<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;</button>';
      html +='<strong> Upload file </strong>';
      html +='<input type="file" name="multipleFiles[]">';
      html +='<input type="hidden" name="upload_type" value="material">';
      html +='</div>';

      $("div#uploadFileContainer").append(html);
    }

  });

</script>


<script>
  var createAllErrors = function() {
    var form = $( this ),
    errorList = $( "ul.errorMessages", form );

    var showAllErrorMessages = function() {
      errorList.empty();

// Find all invalid fields within the form.
var invalidFields = form.find( ":invalid" ).each( function( index, node ) {

// Find the field's corresponding label
var label = $("label[for='"+$(this).attr('id')+"']");
    // Opera incorrectly does not fill the validationMessage property.
    message = node.validationMessage || 'Invalid value.';

    errorList
    .show()
    .append( "<li><span>" + "<strong>" + label.html() + "</strong>"+ "</span> " + "Please fill this" + "</li>" );
  });
};

// Support Safari
form.on( "submit", function( event ) {
  if ( this.checkValidity && !this.checkValidity() ) {
    $( this ).find( ":invalid" ).first().focus();
    event.preventDefault();
  }
});

$( "input[type=submit], button:not([type=button])", form )
.on( "click", showAllErrorMessages);

$( "input", form ).on( "keypress", function( event ) {
  var type = $( this ).attr( "type" );
  if ( /date|email|month|number|search|tel|text|select|time|url|week/.test ( type )
    && event.keyCode == 13 ) {
    showAllErrorMessages();
}
});
};

$( "form" ).each( createAllErrors );
</script>


                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->
@endsection