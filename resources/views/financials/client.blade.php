@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Order ID</th>
                <th>Amount</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
            @foreach($order as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->amount}}</td>
                <td>{{$value->status}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection