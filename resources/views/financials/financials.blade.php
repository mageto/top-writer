@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Order ID</th>
                <th>Amount</th>
            </tr>
        </thead>

        <tbody>
            @foreach($order as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->amount}}</td>
            </tr>
            @endforeach
            <tr>
                <td>Client Pay</td>
                <td>{{$client_pay}}</td>
                <td>Writer Paid</td>
                <td>{{$writer_paid}}</td>
            </tr>
            <tfoot>
                <tr>
                    <td>Profit</td>
                    <td>{{$profit}}</td>
                </tr>
            </tfoot>
        </tbody>
    </table>
</div>
@endsection