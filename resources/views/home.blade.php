@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        @if(Auth::user()->user_type == 'admin')
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Number of Clients</h4>

                <div class="widget-box-2">
                    <div class="widget-detail-2">
                        <!-- <span class="badge badge-success pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span> -->
                        <h2 class="m-b-0"> {{ App\User::where('user_type', 'client')->count() }} </h2>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        @endif
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Completed Orders</h4>

                <div class="widget-box-2">
                    <div class="widget-detail-2">
                        <?php 
                        
                        if(Auth::user()->user_type == 'writer'){
                            $c = App\Order::where('writer_id', Auth::user()->id)->where('status', 'complete')->count();
                            $a = App\Order::where('status', 'assigned')->orWhere('status', 'complete')->count();
                            if ($a == 0) {
                                $p_c = 'none';
                            }else{
                                $p_c = ($c/$a)*100;
                            }
                        }
                        if(Auth::user()->user_type == 'admin'){
                            $c = App\Order::where('status', 'complete')->count();
                            $a = App\Order::count();
                            if ($a == 0) {
                                $p_c = 'none';
                            }else{
                                $p_c = ($c/$a)*100;
                            }
                        }
                        if(Auth::user()->user_type == 'client'){
                            $c = App\Order::where('client_id', Auth::user()->id)->where('status', 'complete')->count();
                            $a = App\Order::where('client_id', Auth::user()->id)->count();
                            if ($a == 0) {
                                $p_c = 'none';
                            }else{
                                $p_c = ($c/$a)*100;
                            }
                        }
                        ?>
                        <span class="badge badge-success pull-left m-t-20">{{round($p_c)}} % <i class="zmdi zmdi-trending-up"></i> </span>
                        <h2 class="m-b-0"> {{ $c }} </h2>
                    </div>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Available Orders</h4>

                <div class="widget-box-2">
                    <div class="widget-detail-2">
                        <?php 

                            
                            if(Auth::user()->user_type == 'client'){
                                $c1 = App\Order::where('client_id', Auth::user()->id)->Where('status', 'open')->count();
                                $a = App\Order::where('client_id', Auth::user()->id)->count();
                                
                                if ($a == 0) {
                                    $p_a = 'none';
                                }else{
                                    $p_a = ($c/$a)*100;
                                }
                                // $p_a = ($c1/$a)*100;
                                $c = App\Order::where('client_id', Auth::user()->id)->Where('status', 'open')->count();
                            }
                            if(Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'writer'){
                                $c1 = App\Order::where('status', 'open')->count();
                                $a = App\Order::count();
                                if ($a == 0) {
                                    $p_a = 'none';
                                }else{
                                    $p_a = ($c/$a)*100;
                                }
                                // $p_a = ($c1/$a)*100;
                                $c = App\Order::Where('status', 'open')->count();
                            }
                        ?>
                        <!-- <span class="badge badge-success pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span> -->
                        <span class="badge badge-pink pull-left m-t-20">{{round($p_a)}} % <i class="zmdi zmdi-trending-up"></i> </span>
                        <h2 class="m-b-0"> {{ $c }} </h2>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        
        @if(Auth::user()->user_type == 'admin')
        <div class="col-lg-3 col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Number Writers</h4>

                <div class="widget-box-2">
                    <div class="widget-detail-2">
                        <h2 class="m-b-0"> {{ App\User::where('user_type', 'writer')->count() }} </h2>
                    </div>
                </div>
            </div>
        </div><!-- end col -->
        @endif
    </div>
    <div class="row">
        <div class="card-box col-sm-12">
            <canvas id="myChart" width="400" height="200"></canvas>
        </div>
    </div>
</div>
<!-- Chart JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script src="{{ asset('assets/pages/jquery.chartjs.init.js') }}"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');

$.ajax({
    url: '/first-chart', // point to server-side PHP script
    // data: { name : name, email : email, phone : phone } ,
    type: 'GET',
    success: function(datas) {
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Canceled Orders', 'Open/Available Orders', 'Assigned/In Progress Orders', 'Completed Orders'],
                datasets: [{
                    label: '# of Orders',
                    data: datas,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'Number of Orders Per Status'
                }
            }
        });},
    error: function (error, data) {
        console.log('Error:', data);
    }
});
</script>
@endsection
