@extends('layouts.app')

@section('content')
    
<div class="card-box">   
    <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Invoice Number</th>
                <th>Writer</th>
                <th>Orders</th>
                <th>Total</th>
                <th>Status</th>
                <th>Date Paid</th>
            </tr>
        </thead>

        <tbody>
            @foreach($invoices as $value)
            <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->invoice_no}}</td>
                <td>{{$value->writers['name']}}</td>
                <td>{{$value->orders}}</td>
                <td>{{$value->total}}</td>
                <td>{{$value->status}}</td>
                <td>{{$value->created_at}}</td>
                <td>
                    <a class="btn btn-xs btn-primary" href="/my-invoice/{{$value->id}}">View</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection