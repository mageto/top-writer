<!DOCTYPE html>
<html lang="en">
<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Writers, Writing, academic writing, referencing styles, creative writing" />
    <meta name="description" content="A Web Portal for writers with direct clients and who manage their own writers." />

    <!-- title  -->
    <title>Writers Web</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custombox/dist/custombox.min.css') }}" rel="stylesheet">
    <!-- plugins -->
    <!-- <link rel="stylesheet" href="{{ asset('landing/plugins.css') }}" /> -->
    <link href="{{ asset('landing/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('landing/fontawesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/owl.carousel.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/default.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/nav-menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('landing/mailform.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <!-- search css -->
    <link rel="stylesheet" href="{{ asset('landing/search.css') }}" />

    <!-- switcher css -->
    <link href="{{ asset('landing/switcher.css') }}" rel="stylesheet">

    <!-- core style css -->
    <link href="{{ asset('landing/styles.css') }}" rel="stylesheet" id="colors" />
    <!-- jQuery  -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>

<style>body{background-image: url("../img/pattren5.jpg");}</style>
</head>

<body>

    <!-- start page loading -->
    <div id="preloader">
        <div class="row loader">
            <div class="loader-icon"></div>
        </div>
    </div>
    <!-- end page loading -->

    <!-- start main-wrapper section -->
    <div class="main-wrapper wrapper-boxed">

        <!-- Start Header -->
        <header class="header onepage-header" data-scroll-index="0">

            <div class="container">

                <div class="menu_area alt-font">

                    <!-- Start Navbar -->
                    <nav style="background:#191919; width:109% !important; margin-left:-50px !important;" class="navbar navbar-expand-lg no-padding">

                        <div class="container sm-position-relative">

                            <div class="navbar-header navbar-header-custom">
                                <!-- start logo -->
                                <a href="/" class="navbar-brand white-logo"><img id="logo" src="{{ asset('img/logo.png') }}" alt="logo"></a>
                                <!-- end logo -->
                            </div>

                            <div class="navbar-toggler"></div>

                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/#1">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/#2">Services</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="/#3">Pricing</a>
                                </li> -->
                                <li class="nav-item">
                                    <a class="nav-link" href="/#6">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <!-- <div class="margin-20px-left sm-display-none"> -->
                                        <a class="nav-link butn blue" href="/login">Login</a>
                                    <!-- </div> -->
                                </li>
                                <li class="nav-item">
                                    <!-- <div class="margin-20px-left sm-display-none"> -->
                                        <a class="nav-link butn blue" href="/order-now">Order Now</a>
                                    <!-- </div> -->
                                </li>
                            </ul>

                        </div>

                    </nav>
                    <!-- End Navbar  -->
                </div>

            </div>

        </header>
        <!-- End Header -->

        <!-- start business service section -->
        <section class="bg-light-gray">
            <div class="container">
                    <h3 class="col-md-offset-5">New Order</h3>
                    
                    @if (session('error') || session('success'))
                    <p class="{{ session('error') ? 'error':'success' }}">
                      {{ session('error') ?? session('success') }}
                    </p>
                    @endif
                    <hr/>
                <div class="row card-box" style="margin-top:30px;">
                    <div class="col-lg-6 col-md-12 sm-text-center sm-margin-20px-bottom">
                        
                    <form class="form-horizontal" action="/new-order" enctype="multipart/form-data" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                            <label class="control-label col-sm-3 text-right" for="title">Title</label>
                            <div class="col-sm-9">
                                <input type="text" name="topic" class="" id="title" placeholder="Enter topic" required>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 text-right" for="subject">Subject (category)</label>
                            <div class="col-sm-9 select">          
                                <select name='subject' id='subject' class="" required >
                                <option value=""> Select Subject </option>
                                <?php foreach ($subject as $key) { ?>
                                    <option value={{ $key->id }}>{{ $key->subject }}</option>
                                <?php } ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 text-right" for="academiclevel">Academic level</label>
                            <div class="col-sm-9 select">          
                                <select id='academic_level' name='academic_level' class=""   required >
                                <option value=""> Select Academic Level </option>
                                <?php foreach ($academics as $key) { ?>
                                    <option value={{ $key->academic_level }}>{{ $key->name }}</option>
                                <?php } ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 text-right" for="words">Word/Pages</label>
                            <div class="col-sm-9 select">          
                                <select id="words" name='words'  class=""   required >
                                <?php for ($x = 1; $x <= 100; $x++) {  ?>
                                    <option value="<?php echo $x; ?>"> <?php echo $x; ?> Pages || Aprox <?php $words=$x*275; echo $words; ?> Words </option>
                                <?php } ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label class="control-label col-sm-3 text-right" for="urgency">Urgency</label>
                            <div class="col-sm-9 select">          
                                <select  id='urgency' name='urgency' class="" required>
                                    <?php foreach ($urgency as $key) { ?>
                                    <option style="text-transform:capitalize;" value="{{ $key->deadline }}">{{ $key->deadline }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="amount">
                                <label class="control-label col-sm-3 text-right" for="">Amount </label>
                                <div class="col-sm-4 select">
                                    <select  id='currency' name='currency' class=" " >
                                            <option value="1">USD</option>
                                            <option value="0.89">EUR</option>
                                            <option value="1.3">AUD</option>
                                            <option value="0.69">GBP</option>
                                        </select>

                                </div>
                                <div class="col-sm-5">
                                    <input type="input" id="amount" class="" name="amount" value="$" placeholder="10" readonly/>
                                    <input type="hidden" id="usdamount" class="" name="usdamount" value="$" placeholder="10" readonly/>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="pwd">Number of Sources</label>
                                <div class="col-sm-9 select">          
                                <select id='sources' name='sources'  class=""  >
                                    <option value="1"> Select number of Sources </option>
                                    <?php for ($x = 1; $x <= 100; $x++) {  ?>
                                    <option value="<?php echo $x; ?>"> <?php echo $x; ?>  </option>
                                    <?php } ?> 
                                </select>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="pwd">Format style</label>
                                <div class="col-sm-9 select">          
                                <select name='referencing_style' id="referencing_style" class=""  >
                                    <option> Select Referencing Style </option>
                                    <option value="MLA">MLA</option>
                                    <option value="HAVARD">HAVARD</option>
                                    <option value="Chicago">Chicago</option>
                                    <option value="APA">APA</option>
                                    <option value="Turabian">Turabian</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="instructions">Instructions</label>
                                <div class="col-sm-9">
                                <textarea class="ckeditor " id='editor'  name="instructions"></textarea>
                                </div>
                            </div>
                            <hr/>
                                <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="name">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class=" form" id="name" placeholder="Enter Name" required>
                                </div>
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="email">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class=" form" id="email" placeholder="Enter Email" required>
                                </div>
                                </div>
                                <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="password">password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" class=" form" id="password" placeholder="Enter password" required>
                                </div>
                                </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-right" for="email">Additional materials</label>
                                <div class="col-sm-9">
                                <button id="ADDFILE" class="btn btn-danger btn-block"> Add/upload file(s)</button>
                                <div id="uploadFileContainer"></div>
                                </div>
                            </div>
                            <div class="form-group">        
                                <div class="col-sm-offset-3 col-sm-9">
                                    <!-- <input type="submit"  class="btn btn-info btn-block" name="submit"/> -->
                                    <a 
                          href="#custom-modal" 
                          class="btn btn-primary btn-block waves-effect waves-light m-r-5 m-b-10" 
                          data-animation="blur" 
                          data-plugin="custommodal"
                          data-overlaySpeed="100"
                          onclick="confirm_order()" 
                          data-overlayColor="#36404a">Submit
                        </a>
                        <button style="display:none;" type="submit" id="submit" name="submit">Proceed to Payment</button>
                          <!-- Modal -->
                          <div id="custom-modal" class="modal-demo">
                              <button type="button" class="close" onclick="Custombox.close();">
                                  <span>&times;</span><span class="sr-only">Close</span>
                              </button>
                              <h4 class="custom-modal-title">Confirm Order</h4>
                              <div class="custom-modal-text">
                                  <div class="card-box">
                                      <table style="width:500px; text-align:left;" class="table m-0">
                                          <thead>
                                              <tr>
                                                  <th colspan="2" >Details</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr>
                                                  <td>Order Topic</td>
                                                  <td id=""><label style="text-transform:capitalize;" id="topics"></label></td>
                                              </tr>
                                              <tr>
                                                  <td>Subject</td>
                                                  <td id=""> <label style="text-transform:capitalize;" id="subjects"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Academic Level</td>
                                                  <td id=""> <label style="text-transform:capitalize;" id="academic_levels"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Words/Pages</td>
                                                  <td id=""> <label id="word"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Urgency</td>
                                                  <td id=""> <label id="urgent"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Referencing Style</td>
                                                  <td id=""> <label id="referencing_styles"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Sources</td>
                                                  <td id=""> <label id="source"></label> </td>
                                              </tr>
                                              <tr>
                                                  <td>Instructions</td>
                                                  <td id=""> <label id="instruction"></label> </td>
                                              </tr>
                                              <tr> 
                                                  <td><strong>Amount</strong> </td>
                                                  <td id=""><strong> <u><label id="amounts"></label></u>  </strong></td>
                                              </tr>
                                              <tr>
                                                <td colspan="2"><button type="submit" onclick="me()" class="btn btn-info btn-block fullwidth">Proceed to Payment</button></td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                            
                          </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        
                    <!-- <div class="col-sm-4"> -->
                    <h3> 100% Custom in 3-steps </h3>
                    <div class="row" style="border: 1px dotted #31b0d5;">
                      <div class="col-sm-3"><h1><i class="fa fa-book fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Fill Order Details</h5><p>  Fill In the Instructions of Your Essays.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-pencil fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Sign Up</h5><p> Sign Up for New clients/Sign In for our regular clients.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-check    fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Confirm</h5><p> Confirm Order is Correct.</p></div>
                      </div>
                      <div class="row elementpad" style="border: 1px dotted #31b0d5;">
                        <div class="col-sm-3"><h1><i class="fa fa-paypal fa-2x"></i></h1></div>
                        <div class="col-sm-9"><h5>Pay for your Order</h5><p>Pay via Paypal/Credit Card.</p></div>
                      </div>
                    </div>
                  
                    </div>
                </div>
            </div>
        </section>
        <!-- end business section -->

        <!-- start footer section -->
        <!-- <footer> -->
            <div class="footer-bar">
                <div class="container">
                    <p class="xs-font-size13">&copy; 2019 </p>
                </div>
            </div>
        <!-- </footer> -->
        <!-- end footer section -->

    </div>
    <!-- end main-wrapper section -->

    <!-- Java script -->
    <script src="{{ asset('landing/core.min.js') }}"></script>

    <!-- serch -->
    <script src="{{ asset('landing/search.js') }}"></script>
    
    <!-- custom scripts -->
    <script src="{{ asset('landing/main.js') }}"></script>

    <!-- contact form scripts -->
    <script src="{{ asset('landing/jquery.form.min.js') }}"></script>
    <script src="{{ asset('landing/jquery.rd-mailform.min.c.js') }}"></script>

    <!-- all js include end -->
    <script>
  function me(){
    // alert('Me was clicked');
    document.getElementById('submit').click();
  }
  function confirm_order(){
    var urgency = document.getElementById('urgency').value;
    var academic_level = document.getElementById('academic_level').value;
    var words = document.getElementById('words').value;
    var topic = document.getElementById('title').value;
    var subject = document.getElementById('subject').value;
    var amount = document.getElementById('amount').value;
    var sources = document.getElementById('sources').value;
    var referencing_style = document.getElementById('referencing_style').value;
    var instructions = document.getElementById('editor').value;

    document.getElementById('urgent').innerHTML = urgency;
    document.getElementById('academic_levels').innerHTML = academic_level;
    document.getElementById('word').innerHTML = words;
    document.getElementById('topics').innerHTML = topic;
    document.getElementById('subjects').innerHTML = subject;
    document.getElementById('amounts').innerHTML = amount;
    document.getElementById('source').innerHTML = sources;
    document.getElementById('referencing_styles').innerHTML = referencing_style;
    document.getElementById('instruction').innerHTML = instructions;
    
  }
</script>

<script type="text/javascript">


$("select[id='words'], select[id='academic_level'], select[id='subject'], input[id='discount'], select[id='currency'], select[id='urgency']").change(function(){

  $q1 = document.getElementById('urgency').value;
  $q2 = document.getElementById('academic_level').value;
  $q3 = document.getElementById('words').value;
  $q4 = document.getElementById('currency').value;
  // alert($q2);
     $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      url : "/calculate",
      type: "get",
      data: {"urgency" : $q1, "academic_level" : $q2, "words" : $q3, "currency" : $q4},
      // dataType: "json",
      success: function(data)
      {
        var dat = JSON.parse(data);
          var h = dat[0][0].highschool;
          var m = dat[0][0].master;
          var u = dat[0][0].undergraduate;
          var d = dat[0][0].doctoral;
          var w = dat[1].words;
          var c = parseFloat(dat[2].currency);
          if (h) {
          document.getElementById('amount').value = h*w*c;
          }
          if (m) {
          document.getElementById('amount').value = m*w*c;
          }
          if (u) {
          document.getElementById('amount').value = u*w*c;
          }
          if (d) {
          document.getElementById('amount').value = d*w*c;
          }
         },
         error: function (jqXHR, textStatus, errorThrown)
         {
          console.log(errorThrown);
         }
     });
});

</script>

<script type="text/javascript">
jQuery(document).ready(function($){

  $(document).on('click', 'button#ADDFILE', function(event) {
    event.preventDefault();
    $("div#submit").css("display", "block")
    addFileInput();
  });

  function addFileInput() {
    var html ='';
    html +='<div class="alert alert-info">';
    html +='<button type="button" class="close" data-dismiss="alert" aria-hidden="true"> &times;</button>';
    // html +='<strong> Upload file </strong>';
    html +='<input type="file" name="multipleFiles[]">';
    html +='<input type="hidden" name="upload_type" value="material">';
    html +='</div>';

    $("div#uploadFileContainer").append(html);
  }

});

</script>


<script>
var createAllErrors = function() {
  var form = $( this ),
  errorList = $( "ul.errorMessages", form );

  var showAllErrorMessages = function() {
    errorList.empty();

// Find all invalid fields within the form.
var invalidFields = form.find( ":invalid" ).each( function( index, node ) {

// Find the field's corresponding label
var label = $("label[for='"+$(this).attr('id')+"']");
  // Opera incorrectly does not fill the validationMessage property.
  message = node.validationMessage || 'Invalid value.';

  errorList
  .show()
  .append( "<li><span>" + "<strong>" + label.html() + "</strong>"+ "</span> " + "Please fill this" + "</li>" );
});
};

// Support Safari
form.on( "submit", function( event ) {
if ( this.checkValidity && !this.checkValidity() ) {
  $( this ).find( ":invalid" ).first().focus();
  event.preventDefault();
}
});

$( "input[type=submit], button:not([type=button])", form )
.on( "click", showAllErrorMessages);

$( "input", form ).on( "keypress", function( event ) {
var type = $( this ).attr( "type" );
if ( /date|email|month|number|search|tel|text|select|time|url|week/.test ( type )
  && event.keyCode == 13 ) {
  showAllErrorMessages();
}
});
};

$( "form" ).each( createAllErrors );
</script>

    <!-- start scroll to top -->
    <a href="javascript:void(0)" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    <!-- end scroll to top -->

    <!-- all js include start -->

    <!-- Java script -->
    <script src="{{ asset('landing/core.min.js') }}"></script>

    <!-- serch -->
    <script src="{{ asset('landing/search.js') }}"></script>
    
    <!-- custom scripts -->
    <script src="{{ asset('landing/main.js') }}"></script>

    <!-- contact form scripts -->
    <script src="{{ asset('landing/jquery.form.min.js') }}"></script>
    <script src="{{ asset('landing/jquery.rd-mailform.min.c.js') }}"></script>

    <!-- all js include end -->

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/detect.js') }}"></script>
        <script src="{{ asset('assets/js/fastclick.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('assets/js/waves.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.scrollTo.min.js') }}"></script>

        <!-- Modal-Effect -->
        <script src="{{ asset('assets/plugins/custombox/dist/custombox.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/custombox/dist/legacy.min.js') }}"></script>
        <!-- file uploads js -->
        <script src="{{ asset('assets/plugins/fileuploads/js/dropify.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/jquery.core.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.app.js') }}"></script>

        <script type="text/javascript">
            $('.dropify').dropify({
                messages: {
                    'default': 'Drag and drop a file here or click',
                    'replace': 'Drag and drop or click to replace',
                    'remove': 'Remove',
                    'error': 'Ooops, something wrong appended.'
                },
                error: {
                    'fileSize': 'The file size is too big (1M max).'
                }
            });
        </script>
</body>
</html>