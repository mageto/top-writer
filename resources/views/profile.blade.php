@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-1">
        <div class="bg-picture card-box">
            <div class="profile-info-name col-sm-offset-3">
                @if(Auth::user()->avatar != null)
                    <img src="{{asset('avatar-files/')}}/{{Auth::user()->avatar}}" class="img-thumbnail" alt="profile-image">
                @else
                    <img src="{{ asset('img/user.png') }}" alt="user-img" class="img-circle user-img">
                @endif
            </div>
            <div class="clearfix"></div>
            <div class="m-t-10 profile-info-detail">
                <div class="panel panel-color panel-inverse">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{Auth::user()->name}}</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table m-b-0">
                            <tbody>
                                <tr>
                                    <th>Email</th>
                                    <td>{{Auth::user()->email}}</td>
                                </tr>

                                <tr>
                                    <th>Phone</th>
                                    <td>{{Auth::user()->phone}}</td>
                                </tr>

                                <tr>
                                    <th>Address</th>
                                    <td>{{Auth::user()->address}}</td>
                                </tr>

                                <tr>
                                    <th>Gender</th>
                                    <td>{{Auth::user()->gender}}</td>
                                </tr>

                                <tr>
                                    <th>User Type</th>
                                    <td>{{Auth::user()->user_type}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
        <!-- </div>
        <div class="card-box"> -->
            <h3>Update Avatar</h3>
            <form action="/update-avatar" enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                </div>
                <div class="form-group">
                    <input type="file" name="avatar" id="avatar">
                </div>
                <button type="submit" class="btn btn-success btn-block btn-sm waves-effect waves-light">
                    Upload
                </button>
            </form>
        </div>
        <!--/ meta -->
    </div>

    <div class="col-sm-6">
        <div class="">
            <div class="panel panel-color panel-inverse">
                <div class="panel-heading">
                    <h3 class="panel-title">Update profile</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="/profile-update" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{Auth::user()->phone}}">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" value="{{Auth::user()->address}}">
                        </div>
                        <div class="form-group">
                            <label for="gender">Gender</label>
                            <select class="form-control" name="gender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="password">Change Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>     
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
