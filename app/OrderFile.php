<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderFile extends Model
{
    use SoftDeletes;
    
    protected $table = 'order_files';

    protected $fillable = [
        'order_id', 'file_name'
    ];
}
