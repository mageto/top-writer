<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Order;
use App\Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    //  */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function financial()
    {
        $user_id = Auth::user()->id;
        $user = Auth::user()->user_type;
        if ($user == 'admin') {
            $order = Order::where('status', 'Complete')->get();
            $writer_paid = Order::where('status', 'Complete')->where('writer_paid', 'paid')->sum('writer_pay');
            $client_pay = Order::where('status', 'Complete')->sum('amount');
            $profit = $client_pay - $writer_paid;
            return view('financials.financials', compact('order','writer_paid','client_pay', 'profit'));
        } elseif ($user == 'writer') {
            $order = Order::where('writer_id', $user_id)->where('writer_paid', 'paid')->where('status', 'Complete')->get();
            $writer_paid = Order::where('writer_id', $user_id)->where('status', 'Complete')->sum('writer_pay');
            return view('financials.writer', compact('order','writer_paid'));
        } elseif ($user == 'client') {
            $order = Order::where('client_id', $user_id)->get();
            return view('financials.client', compact('order'));
        } {
            
        }
        
    }
    public function msg_config()
    {
        return view('config');
    }
    public function email_config(Request $request)
    {
        Config::where('id', 1)->update([ 'name' => $request->name, 'email' => $request->email]);
        return redirect()->back();
    }
}
