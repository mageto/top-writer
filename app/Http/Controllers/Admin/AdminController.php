<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\User;
use App\Order;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        
        return view('client.dashboard');
    }
    function order(){
        $subject   =  DB::table('subjects')->get();
        $academics =  DB::table('academic_level')->get();
        $urgency   =  DB::table('custom_prices')->get();
        return view('client.order', compact('subject', 'academics', 'urgency'));
    }
    function orders(){
        $orders = Order::with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }
    function open(){
        $orders = Order::where('status', 'Open')->with('orderFiles','subjects','clients')->get();
    //    echo json_encode($orders); die();
        return view('admin.orders', compact('orders'));
    }
    function completed(){
        $orders = Order::where('status', 'Complete')->with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }
    function assigned(){
        $orders = Order::where('status', 'Assigned')->with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }
    function revisions(){
        $orders = Order::where('status', 'Revision')->with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }
    function canceled(){
        $orders = Order::where('status', 'Canceled')->with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }
    public function profile(){
        return view('profile');
    }
    public function update_avatar(Request $request){
        $user_id = $request->input('user_id');
        $file    = $request->file('avatar');

        $filename = time().'-'.$user_id.'-'.$file->getClientOriginalName();
        $file->move('avatar-files', $filename);
        
        User::where('id', $user_id)->update(['avatar' => $filename]);
        return redirect()->back();
    }
    public function profile_update(Request $request){
        
        if ($request->input('password') == null) {
            User::where('id', $request->input('user_id'))->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'address' => $request->input('address'),
                    'gender' => $request->input('gender')
                ]);
            return redirect()->back();
        } else {
            User::where('id', $request->input('user_id'))->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'address' => $request->input('address'),
                    'gender' => $request->input('gender'),
                    'password' => bcrypt($request->input('password'))
                ]);
            return redirect()->back();
        }
    }
    public function first_chart(){
        $user_type = Auth::user()->user_type;
        $user_id   = Auth::user()->id;
        if ($user_type == 'admin') {
            $c  = Order::where('status', 'Canceled')->count();
            $o  = Order::where('status', 'Open')->count();
            $a  = Order::where('status', 'Assigned')->count();
            $co = Order::where('status', 'Complete')->count();

            $data = [$c, $o, $a, $co ];

            return $data;
            
        } else if ($user_type == 'writer') {
            $c  = Order::where('writer_id', $user_id)->where('status', 'Canceled')->count();
            $o  = Order::where('writer_id', $user_id)->where('status', 'Open')->count();
            $a  = Order::where('writer_id', $user_id)->where('status', 'Assigned')->count();
            $co = Order::where('writer_id', $user_id)->where('status', 'Complete')->count();

            $data = [$c, $o, $a, $co ];

            return $data;
            
        }else{
            $c  = Order::where('client_id', $user_id)->where('status', 'Canceled')->count();
            $o  = Order::where('client_id', $user_id)->where('status', 'Open')->count();
            $a  = Order::where('client_id', $user_id)->where('status', 'Assigned')->count();
            $co = Order::where('client_id', $user_id)->where('status', 'Complete')->count();

            $data = [$c, $o, $a, $co ];

            return $data;
            
        }
    }
}
