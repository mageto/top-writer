<?php

namespace App\Http\Controllers\Writer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Mail;
use App\User;
use App\Order;
use App\Invoice;
use App\WriterOrderFile;
class WriterController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    function index(){
        
        return view('client.dashboard');
    }
    public function writers(){
        $writers = User::where('user_type', 'writer')->get();
        return view('writer.writers', compact('writers'));
    }
    public function writer($id){
        $writer = User::where('id', $id)->get();
        return view('writer.writer', compact('writer'));
    }
    public function delete_writer($id){
        return User::where('id', $id)->delete();
    }
    public function invoice($id){
        $writer = User::where('id', $id)->get();
        $invoice = Order::where('writer_id', $id)->where('status', 'Complete')->get();
        $total = Order::where('writer_id', $id)->where('status', 'Complete')->sum('writer_pay');
        $invoice_no = date('Y-m-d').'-'.$id;
        $writer_id = $id;
        $order = [];
        foreach ($invoice as $value) {
            $id = $value->id;
            array_push($order, $id);
        }
        $orders = json_encode($order);
        return view('writer.invoice', compact('orders','writer_id','invoice','total','writer', 'invoice_no'));
    }
    public function add_invoice( Request $request){
        $invoice = Invoice::create([
            'invoice_no'    => $request->invoice_no,
            'writer_id'     => $request->writer_id,
            'orders'        => $request->orders,
            'total'         => $request->total
        ]);
        return $invoice;
    }
    public function invoices( Request $request){
        $user    = Auth::user()->user_type; 
        $user_id = Auth::user()->id;

        if ($user == 'admin') {
            $invoices = Invoice::with('writers')->get();
        } else {
            $invoices = Invoice::where('writer_id', $user_id)->get();
        }
        return view('invoices', compact('invoices'));
    }
    public function my_invoice($id){
        $invoice = Invoice::where('id', $id)->with('writers')->get();

        return view('invoice', compact('invoice'));
    }
    public function add_writer(Request $request){
        $writer = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'user_type' => 'writer',
            'password' => bcrypt($request->input('password')),
        ]);
        // send email
        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'siteurl'            =>   $siteurl,
            'name'               =>   $request->input('name')
        ];
        Mail::send('email.new_writer',$data, function($message) use ($data){
            $message->to($request->input('email'),$request->input('name'))->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return $writer;
    }
    public function completed_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('status', 'Complete')->where('writer_id', $user_id)->with('orderFiles','subjects')->get();
        $client = Order::where('status', 'Complete')->where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;

        return view('writer.completed', compact('orders'));
    }
    public function assigned_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('status', 'Assigned')->where('writer_id', $user_id)->with('orderFiles','subjects')->get();
        $client = Order::where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;
        
        return view('writer.assigned', compact('orders'));
    }
    public function open_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('status', 'Open')->with('orderFiles','subjects')->orderBy('id', 'DESC')->get();
        $client = Order::where('status', 'Open')->where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;
        
        return view('writer.open', compact('orders'));
    }
    public function unpaid_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('writer_id', $user_id)->where('writer_paid', 'unpaid')->with('orderFiles','subjects')->get();
        $client = Order::where('amount', null)->where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;
        
        return view('writer.unpaid', compact('orders'));
    }
    public function revision_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('status', 'Revision')->where('writer_id', $user_id)->with('orderFiles','subjects')->get();
        $client = Order::where('status', 'Revision')->where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;
        
        return view('writer.revision', compact('orders'));
    }
    public function canceled_orders(){
        $user_id = Auth::user()->id;
        $user    = Auth::user()->user_type;
        $writer = Order::where('status', 'Canceled')->where('writer_id', $user_id)->with('orderFiles','subjects')->get();
        $client = Order::where('status', 'Canceled')->where('client_id', $user_id)->with('orderFiles','subjects')->get();
        
        $orders = ($user == 'writer') ? $writer : $client ;
        
        return view('writer.canceled', compact('orders'));
    }
    public function writer_files(Request $request){
        $order_id = $request->input('order_id');
        $files    = $request->file('multipleFiles');
        // var_dump($files);
        // die();
        foreach ($files as $file)
        {
            $filename = time().'-'.$order_id.'-'.$file->getClientOriginalName();
            $file->move('writer-order-files', $filename);
            
            WriterOrderFile::create([
                'order_id' => $order_id,
                'file_name' => $filename
            ]);
        }
        return redirect()->back();
    }
}
