<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use App\User;
use App\Order;
class ClientController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    function index(){
        
        return view('client.dashboard');
    }
    public function contact_form(Request $request){
        // $name                   = User::where('id', $user_id)->value('name');
        $siteurl        =   DB::table('configs')->where('id', 1)->value('website');
        $email_to             =   DB::table('configs')->where('id', 1)->value('email');
         
        $to      = $email_to;
        $subject = 'Contact Form';
        $message = 'Email from:'.$siteurl. "\r\n" .
                    'Name:'.$request->input('name'). "\r\n" .
                    'Phone:'.$request->input('phone'). "\r\n" .
                    $request->input('message');
        $headers = 'From:'. $request->input('email') . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();;
        
        mail($to, $subject, $message, $headers);
        // return redirect()->back();
        
    }
    public function clients(){
        $clients = User::where('user_type', 'client')->get();
        return view('client.clients', compact('clients'));
    }
    public function client($id){
        $client = User::where('id', $id)->get();
        return view('client.client', compact('client'));
    }
    public function delete_client($id){
        return User::where('id', $id)->delete();
    }
    public function calculate(Request $request){

        $urgency = $request->input('urgency');
        $academiclevel = $request->input('academic_level');
        $words['words'] = $request->input('words');
        $currency['currency'] = $request->input('currency');


        $sql = DB::table('custom_prices')
                ->where('deadline', $urgency)
                ->select($academiclevel)
                ->get();
        // $sql = $this->db->query("SELECT `$academiclevel` FROM `custom_prices` WHERE `deadline` = '$urgency'");
        // $data = $sql->result(); 
        
        $f = [$sql,$words, $currency];

        return json_encode($f);
    }
    public function revision_orders(){
        $orders = Order::where('status', 'Revision')->where('client_id', Auth::user()->id)->with('orderFiles','subjects')->get();
                return view('client.revision', compact('orders'));
    }
    public function completed_orders(){
        $orders = Order::where('status', 'Completed')->where('client_id', Auth::user()->id)->with('orderFiles','subjects')->get();
                return view('client.completed', compact('orders'));
    }
    public function canceled_orders(){
        $orders = Order::where('status', 'Canceled')->where('client_id', Auth::user()->id)->with('orderFiles','subjects')->get();
                return view('client.canceled', compact('orders'));
    }
    public function rate_order(Request $request){
        return Order::where('id', $request->id)->update(['rate' => $request->rate]);
    }
}
