<?php
namespace App\Http\Controllers;
use App\User;
use Mail;
use DB;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class MessageController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        // All threads, ignore deleted/archived participants
        // $threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        return view('messenger.index', compact('threads'));
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->route('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $admin_users = User::whereNotIn('id', $thread->participantsUserIds($userId))
                            ->where('user_type', 'admin')->get();
        $thread->markAsRead($userId);
        return view('messenger.show', compact('thread', 'users', 'admin_users'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        $admin_users = User::where('user_type', 'admin')->get();
        return view('messenger.create', compact('users', 'admin_users'));
    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();
        $thread = Thread::create([
            'subject' => $input['subject'],
        ]);
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $input['message'],
        ]);
        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }
        //  $name                   = User::where('id', $user_id)->value('name');
        // $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        // $email                  = User::where('id', $request->input('user_id'))->value('email');
        // $message                = $request->input('message');
        // $subject                = $request->input('subject');
        
        // $data = [
        //     'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
        //     'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
        //     'siteurl'            =>   $siteurl,
        //     'mailmessage'        =>   $message,
        //     'subject'            =>   $subject,
        //     'name'               =>   $name,
        //     'email'              =>   $email
        // ];
        // Mail::send('email.user',$data, function($message) use ($data){
        //     $message->to($data['email'],$data['name'])->subject($data['subject']);
        //     $message->from($data['sender'],$data['sendername']);
        // }); 
        // $to      = 'nobody@example.com';
        // $subject = 'the subject';
        // $message = 'hello';
        // $headers = 'From: webmaster@example.com' . "\r\n" .
        //     'Reply-To: webmaster@example.com' . "\r\n" .
        //     'X-Mailer: PHP/' . phpversion();
        
        // mail($to, $subject, $message, $headers);
        return redirect()->route('messages');
    }
    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->route('messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);
        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }
        return redirect()->route('messages.show', $id);
    }

    public function email(){
        $users = User::where('user_type', '!=', 'admin')->get();
        return view('email', compact('users'));
    }
    public function send_email(Request $request){
        
        $name                   = User::where('id', $user_id)->value('name');
        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $email                  = User::where('id', $request->input('user_id'))->value('email');
        $message                = $request->input('message');
        $subject                = $request->input('subject');
        
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'siteurl'            =>   $siteurl,
            'mailmessage'        =>   $message,
            'subject'            =>   $subject,
            'name'               =>   $name,
            'email'              =>   $email
        ];
        Mail::send('email.user',$data, function($message) use ($data){
            $message->to($data['email'],$data['name'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 

        return redirect()->back();
    }
}