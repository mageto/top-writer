<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Mail;
use App\Order;
use App\OrderFile;
use App\User;
/** Paypal Details classes **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
class OrderController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function __construct()
    {
        $this->api_context = new ApiContext(
            new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret'))
        );
        $this->api_context->setConfig(config('paypal.settings'));
    }
    function index(){
         
    }
    function order(){
        $subject   =  DB::table('subjects')->get();
        $academics =  DB::table('academic_level')->get();
        $urgency   =  DB::table('custom_prices')->get();
        return view('client.order', compact('subject', 'academics', 'urgency'));
    }
    function orders(){
        $orders = Order::with('orderFiles','subjects','clients')->get();
        return view('admin.orders', compact('orders'));
    }

    public function calculate(Request $request){

        $urgency = $request->input('urgency');
        $academiclevel = $request->input('academic_level');
        $words['words'] = $request->input('words');
        $currency['currency'] = $request->input('currency');


        $sql = DB::table('custom_prices')
                ->where('deadline', $urgency)
                ->select($academiclevel)
                ->get();
        // $sql = $this->db->query("SELECT `$academiclevel` FROM `custom_prices` WHERE `deadline` = '$urgency'");
        // $data = $sql->result(); 
        
        $f = [$sql,$words, $currency];

        return json_encode($f);
    }
    public function new_order(Request $request){
        // dd($files);
        $client = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'user_type' => 'client',
            'password' => bcrypt($request->input('password')),
        ]);

        $client_id = $client->id;

        $data = Order::create([
            'topic' => $request->input('topic'),
            'client_id' => $client_id,
            'subject' => $request->input('subject'),
            'academic_level' => $request->input('academic_level'),
            'currency' => $request->input('currency'),
            'words' => $request->input('words'),
            'instructions' => $request->input('instructions'),
            'amount' => $request->input('amount'),
            'urgency' => $request->input('urgency'),
            'status' => "Open",
            'referencing_style' => $request->input('referencing_style'),
            'sources' => $request->input('sources'),
            'writer_paid' => 'unpaid',
            // 'deadline' => $deadline,
            // 'order_period' => $order_period

        ]);

        $order_id = $data->id;
        $files = $request->file('multipleFiles');
        foreach ($files as $file)
        {
            $filename = time().'-'.$order_id.'-'.$file->getClientOriginalName();
            $file->move('order-files', $filename);
            
            OrderFile::create([
                'order_id' => $order_id,
                'file_name' => $filename
            ]);
        }

        $client                 = $client_id = $client->name;
        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $orderNo                = $order_id;
        $email                  = $client_id = $client->email;
        $message                = "A new project/order by a new client has been submitted on your site ". $siteurl;
        
        $data = [
            'topic'              => $request->input('topic'),
            'client_id'          => $client_id,
            'subject'            => $request->input('subject'),
            'academic_level'     => $request->input('academic_level'),
            'currency'           => $request->input('currency'),
            'words'              => $request->input('words'),
            'instructions'       => $request->input('instructions'),
            'amount'             => $request->input('amount'),
            'urgency'            => $request->input('urgency'),
            'deadline'           => date("Y-m-d H:i:s"),
            'status'             => "Open",
            'referencing_style'  => $request->input('referencing_style'),
            'sources'            => $request->input('sources'),

            'name'               =>   $client,
            'to'                 =>   $email,
            'subject'            =>   "New Order",
            'mailmessage'        =>   $message,
            'siteurl'            =>   $siteurl,
            'orderNo'            =>   $orderNo,
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'template'           =>   "email.order"
        ];

        Mail::send('email.order',$data, function($message) use ($data){
            $message->to($data['to'],$data['name'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        });

        Mail::send('email.client',$data, function($message) use ($data){
            $message->to($client->email,$client->name)->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        });
        
        return redirect()->route('login');
    }
    public function add_order(Request $request){
        $client_id = Auth::user()->id;

        $order_data = Order::create([
            'topic' => $request->input('topic'),
            'client_id' => $client_id,
            'subject' => $request->input('subject'),
            'academic_level' => $request->input('academic_level'),
            'currency' => $request->input('currency'),
            'words' => $request->input('words'),
            'instructions' => $request->input('instructions'),
            'amount' => $request->input('amount'),
            'urgency' => $request->input('urgency'),
            'status' => "Open",
            'referencing_style' => $request->input('referencing_style'),
            'sources' => $request->input('sources'),
            'writer_paid' => 'unpaid',
            'deadline' => date("Y-m-d H:i:s"),
            // 'order_period' => $order_period

        ]);

        $order_id = $order_data->id;
        $files = $request->file('multipleFiles');
        foreach ($files as $file)
        {
            $filename = time().'-'.$order_id.'-'.$file->getClientOriginalName();
            $file->move('order-files', $filename);
            
            OrderFile::create([
                'order_id' => $order_id,
                'file_name' => $filename
            ]);
        }

        $client                 = User::where('id', Auth::user()->id)->value('name');
        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $orderNo                = $order_id;
        $email                  = User::where('id', Auth::user()->id)->value('email');
        $message                = "A new project/order has been submitted on your site ". $siteurl;
        
        $data = [
            'topic'              => $request->input('topic'),
            'client_id'          => $client_id,
            'subject'            => $request->input('subject'),
            'academic_level'     => $request->input('academic_level'),
            'currency'           => $request->input('currency'),
            'words'              => $request->input('words'),
            'instructions'       => $request->input('instructions'),
            'amount'             => $request->input('amount'),
            'urgency'            => $request->input('urgency'),
            'deadline'           => date("Y-m-d H:i:s"),
            'status'             => "Open",
            'referencing_style'  => $request->input('referencing_style'),
            'sources'            => $request->input('sources'),

            'name'               =>   $client,
            'to'                 =>   $email,
            'subject'            =>   "New Order",
            'mailmessage'        =>   $message,
            'siteurl'            =>   $siteurl,
            'orderNo'            =>   $orderNo,
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'template'           =>   "email.order"
        ];

        Mail::send('email.order',$data, function($message) use ($data){
            $message->to($data['to'],$data['name'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        });

        Mail::send('email.client',$data, function($message) use ($data){
            $message->to(Auth::user()->email,Auth::user()->name)->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        });
        return redirect()->back();
    }
    public function my_orders(){
        $orders = Order::where('client_id', Auth::user()->id)->with('orderFiles','subjects')->get();
        // echo json_encode($orders);

        return view('client.my_orders', compact('orders'));
    }
    public function my_order($id){
        $orders = Order::where('id', $id)->with('orderFiles','subjects')->get();
        $rate = Order::where('id', $id)->value('rate');
        $writers = User::where('user_type', 'writer')->get();
        return view('client.my_order', compact('rate','orders','writers', 'id'));
    }

    public function assign_order(Request $request){
        $order_id = $request->input('order_id');
        $writer_id = $request->input('writer_id');
        $writer_pay = $request->input('writer_pay');

        Order::where('id', $order_id)
               ->update([ 'writer_id' => $writer_id, 'writer_pay' => $writer_pay, 'status' => 'Assigned' ]);
        
        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'subject'            =>   'Assigned Order',
            'site_url'           =>   $siteurl,
            'order_no'           =>   $order_id,
            'writer_id'          =>   $writer_id,
            'name'               =>   User::where('id', $writer_id)->value('name')
        ];
        Mail::send('email.writer_new_order',$data, function($message) use ($data){
            $message->to(User::where('id', $data['writer_id'])->value('email'),User::where('id', $data['writer_id'])->value('name'))->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return redirect()->back();
    }

    public function completed_order_writer(Request $request){
        $order_id = $request->input('order_id');

        Order::where('id', $order_id)
               ->update(['status' => 'Edit' ]);

        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'subject'            =>   'Assigned Order',
            'site_url'            =>   $siteurl,
            'order_no'           =>   $order_id
        ];
        Mail::send('email.writer_completed_order',$data, function($message) use ($data){
            $message->to($data['sender'],$data['sendername'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return redirect()->back();
    }
    public function completed_order(Request $request){
        $order_id = $request->input('order_id');

        Order::where('id', $order_id)
               ->update(['status' => 'Complete' ]);

        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'subject'            =>   'Assigned Order',
            'site_url'            =>   $siteurl,
            'order_no'           =>   $order_id
        ];
        Mail::send('email.writer_completed_order',$data, function($message) use ($data){
            $message->to($data['sender'],$data['sendername'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return redirect()->back();
    }
    public function cancel_order($id){
        $cancel = Order::where('id', $id)->update(['status' => 'Canceled' ]);

        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'subject'            =>   'Assigned Order',
            'site_url'            =>   $siteurl,
            'order_no'           =>   $id,
            'name'               =>   Auth::user()->name
        ];
        Mail::send('email.client_canceled_order',$data, function($message) use ($data){
            $message->to($data['sender'],$data['sendername'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return $cancel;
    }
    public function delete_order($id){
        Order::where('id', $id)->update(['status' => 'Deleted']);
        return Order::where('id', $id)->delete();
    }
    public function revision_order(Request $request){
        $order_id = $request->input('order_id');

        Order::where('id', $order_id)
               ->update(['status' => 'Revision' ]);

        $siteurl                = DB::table('configs')->where('id', 1)->value('website');
        $data = [
            'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
            'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
            'subject'            =>   'Assigned Order',
            'site_url'            =>   $siteurl,
            'order_no'           =>   $order_id
        ];
        Mail::send('email.writer_revision_order',$data, function($message) use ($data){
            $message->to($data['sender'],$data['sendername'])->subject($data['subject']);
            $message->from($data['sender'],$data['sendername']);
        }); 
        return redirect()->back();
    }

    /** This method sets up the paypal payment. **/
    public function createPayment(Request $request){
        $data = $request->all();
        $c = $request->input('currency');
        if ($c == 1) {
            $currency = 'USD';
        } else if($c == 1.1) {
            $currency = 'EUR';
        } else if($c == 1.3) {
            $currency = 'AUD';
        } else if($c == 0.69) {
            $currency = 'GBP';
        }else{
            $currency = 'USD';
        }
        
        $request->validate(['amount' => 'required|numeric']);
        $pay_amount = $request->amount;
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item = new Item();
        $item->setName('Paypal Payment')->setCurrency($currency)->setQuantity(1)->setPrice($pay_amount);
        $itemList = new ItemList();
        $itemList->setItems(array($item));
        $amount = new Amount();
        $amount->setCurrency($currency)->setTotal($pay_amount);
        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($itemList)
        ->setDescription('Laravel Paypal Payment Tutorial');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('confirm-payment', $data))->setCancelUrl(url()->current());

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));
        try {
            $payment->create($this->api_context);
        } catch (PayPalConnectionException $ex){
            return back()->withError('Some error occur, sorry for the inconvenience.');
        } catch (Exception $ex) {
            return back()->withError('Some error occur, sorry for the inconvinience.');
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        if(isset($redirect_url)) {return redirect($redirect_url);}
        return redirect()->back()->withError('Unknown error occurred');
    }
    /** This method confirms if payment with paypal was processed successful and then execute the payment, 
    ** we have 'paymentId, PayerID and token' in query string. **/
    public function confirmPayment(Request $request){

        if (empty($request->query('paymentId')) || empty($request->query('PayerID')) || empty($request->query('token')))
        return redirect()->back()->withError('Payment was not successful.');
        $payment = Payment::get($request->query('paymentId'), $this->api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->query('PayerID'));
        $result = $payment->execute($execution, $this->api_context);

        if ($result->getState() != 'approved'){
            return redirect()->back()->withError('Payment was not successful.');
        }else{
            $u = $request->urgency;
            $added_date=date("Y-m-d H:i:s",strtotime($request->urgency));
            $deadline = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($added_date)));
            if ($u == '3 hours'||$u == '6 hours'||$u == '8 hours'||$u == '12 hours') {
            $deadline_writers = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($added_date)));
            } else {
                $deadline_writers = date("Y-m-d H:i:s", strtotime('-12 hours', strtotime($deadline)));
            }
            $client_id = Auth::user()->id;

            $order_data = Order::create([
                'topic' => $request->topic,
                'client_id' => $client_id,
                'subject' => $request->subject,
                'academic_level' => $request->academic_level,
                'currency' => $request->currency,
                'words' => $request->words,
                'instructions' => $request->instructions,
                'amount' => $request->amount,
                'urgency' => $request->urgency,
                'status' => "Open",
                'referencing_style' => $request->referencing_style,
                'sources' => $request->sources,
                'writer_paid' => 'unpaid',
                'deadline' => $deadline,
                'deadline_writers' => $deadline_writers,
                'order_period' => $request->urgency
    
            ]);
    
            $order_id = $order_data->id;
            $files = $request->file('multipleFiles');
            
            if ($files != null) {
                foreach ($files as $file)
                {
                    $filename = time().'-'.$order_id.'-'.$file->getClientOriginalName();
                    $file->move('order-files', $filename);
                    
                    OrderFile::create([
                        'order_id' => $order_id,
                        'file_name' => $filename
                    ]);
                }
            }
    
            $client                 = User::where('id', Auth::user()->id)->value('name');
            $siteurl                = DB::table('configs')->where('id', 1)->value('website');
            $orderNo                = $order_id;
            $email                  = User::where('id', Auth::user()->id)->value('email');
            $message                = "A new project/order has been submitted on your site ". $siteurl;
            
            $data = [
                'topic'              => $request->topic,
                'client_id'          => $client_id,
                'subject'            => $request->subject,
                'academic_level'     => $request->academic_level,
                'currency'           => $request->currency,
                'words'              => $request->words,
                'instructions'       => $request->instructions,
                'amount'             => $request->amount,
                'urgency'            => $request->urgency,
                'deadline'           => $deadline,
                'deadline_writers'   => $deadline_writers,
                'status'             => "Open",
                'referencing_style'  => $request->referencing_style,
                'sources'            => $request->sources,
                'name'               => $client,
                'to'                 => $email,
                'subject'            => "New Order",
                'mailmessage'        => $message,
                'siteurl'            => $siteurl,
                'orderNo'            => $orderNo,
                'sender'             => DB::table('configs')->where('id', 1)->value('email'),
                'sendername'         => DB::table('configs')->where('id', 1)->value('name')
            ];
    
            Mail::send('email.order',$data, function($message) use ($data){
                $message->to($data['to'],$data['name'])->subject($data['subject']);
                $message->from($data['sender'],$data['sendername']);
            });
    
            Mail::send('email.client',$data, function($message) use ($data){
                $message->to(Auth::user()->email,Auth::user()->name)->subject($data['subject']);
                $message->from($data['sender'],$data['sendername']);
            });      
            return redirect('/order')->withSuccess('Payment made successfully');
        } 
    }
}
