<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;
use Mail;
use App\Order;
use App\OrderFile;
use App\User;
/** Paypal Details classes **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
class PaymentController extends Controller
{
    private $api_context;
/** 
    ** We declare the Api context as above and initialize it in the contructor
    **/
    public function __construct()
    {
        $this->api_context = new ApiContext(
            new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret'))
        );
        $this->api_context->setConfig(config('paypal.settings'));
    }
/**
    ** This method sets up the paypal payment.
    **/
    public function createPayment(Request $request)
    {   
        $data = $request->all();
        $c = $request->input('currency');
        if ($c == 1) {
            $currency = 'USD';
        } else if($c == 1.1) {
            $currency = 'EUR';
        } else if($c == 1.3) {
            $currency = 'AUD';
        } else if($c == 0.69) {
            $currency = 'GBP';
        }else{
            $currency = 'USD';
        }
        
        $request->validate(['amount' => 'required|numeric']);
        $pay_amount = $request->amount;
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item = new Item();
        $item->setName('Paypal Payment')->setCurrency($currency)->setQuantity(1)->setPrice($pay_amount);
        $itemList = new ItemList();
        $itemList->setItems(array($item));
        $amount = new Amount();
        $amount->setCurrency($currency)->setTotal($pay_amount);
        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($itemList)
        ->setDescription('Laravel Paypal Payment Tutorial');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('confirm-new-payment', $data))->setCancelUrl(url()->current());

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));
        try {
            $payment->create($this->api_context);
        } catch (PayPalConnectionException $ex){
            return back()->withError('Some error occur, sorry for the inconvenience.');
        } catch (Exception $ex) {
            return back()->withError('Some error occur, sorry for the inconvinience.');
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        // You can set a custom data in a session
        // $request->session()->put('key', 'value');;
        // We redirect to paypal to make payment
        if(isset($redirect_url)) { return redirect($redirect_url);}
        // If we don't have redirect url, we have unknown error.
        return redirect()->back()->withError('Unknown error occurred');
    }
/**
    ** This method confirms if payment with paypal was processed successful and then execute the payment, 
    ** we have 'paymentId, PayerID and token' in query string.
    **/
    public function confirmPayment(Request $request)
    {
        // If query data not available... no payments was made.
        if (empty($request->query('paymentId')) || empty($request->query('PayerID')) || empty($request->query('token')))
        return redirect()->back()->withError('Payment was not successful.');
// We retrieve the payment from the paymentId.
        $payment = Payment::get($request->query('paymentId'), $this->api_context);
// We create a payment execution with the PayerId
        $execution = new PaymentExecution();
        $execution->setPayerId($request->query('PayerID'));
// Then we execute the payment.
        $result = $payment->execute($execution, $this->api_context);
// Get value store in array and verified data integrity
        // $value = $request->session()->pull('key', 'default');
// Check if payment is approved
        if ($result->getState() != 'approved'){
            return redirect('/order-now')->withError('Payment was not successful.');
            }else{
        // echo json_encode($request->all());
        // die();
                $u = $request->urgency;
                $added_date=date("Y-m-d H:i:s",strtotime($request->urgency));
                $deadline = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($added_date)));
                if ($u == '3 hours'||$u == '6 hours'||$u == '8 hours'||$u == '12 hours') {
                $deadline_writers = date("Y-m-d H:i:s", strtotime('+3 hours', strtotime($added_date)));
                } else {
                    $deadline_writers = date("Y-m-d H:i:s", strtotime('-12 hours', strtotime($deadline)));
                }
                $client = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'user_type' => 'client',
                    'password' => bcrypt($request->password),
                ]);

                $client_id = $client->id;

                $data = Order::create([
                    'topic' => $request->topic,
                    'client_id' => $client_id,
                    'subject' => $request->subject,
                    'academic_level' => $request->academic_level,
                    'currency' => $request->currency,
                    'words' => $request->words,
                    'instructions' => $request->instructions,
                    'amount' => $request->amount,
                    'urgency' => $request->urgency,
                    'status' => "Open",
                    'referencing_style' => $request->referencing_style,
                    'sources' => $request->sources,
                    'writer_paid' => 'unpaid',
                    'deadline' => $deadline,
                    'deadline_writers' => $deadline_writers,
                    'order_period' => $request->urgency

                ]);

                $order_id = $data->id;
                $files = $request->file('multipleFiles');
                if ($files != null) {
                    foreach ($files as $file)
                    {
                        $filename = time().'-'.$order_id.'-'.$file->getClientOriginalName();
                        $file->move('order-files', $filename);
                        
                        OrderFile::create([
                            'order_id' => $order_id,
                            'file_name' => $filename
                        ]);
                    }
                }
                
                $client                 = User::where('id', $client_id)->value('name');
                $siteurl                = DB::table('configs')->where('id', 1)->value('website');
                $orderNo                = $order_id;
                $email                  = User::where('id', $client_id)->value('email');
                $message                = "A new project/order by a new client has been submitted on your site ". $siteurl;
                
                $data = [
                    'topic'              => $request->topic,
                    'client_id'          => $client_id,
                    'subject'            => $request->subject,
                    'academic_level'     => $request->academic_level,
                    'currency'           => $request->currency,
                    'words'              => $request->words,
                    'instructions'       => $request->instructions,
                    'amount'             => $request->amount,
                    'urgency'            => $request->urgency,
                    'deadline'           => date("Y-m-d H:i:s"),
                    'status'             => "Open",
                    'referencing_style'  => $request->referencing_style,
                    'sources'            => $request->sources,

                    'name'               =>   $client,
                    'to'                 =>   $email,
                    'subject'            =>   "New Order",
                    'mailmessage'        =>   $message,
                    'siteurl'            =>   $siteurl,
                    'orderNo'            =>   $orderNo,
                    'sender'             =>   DB::table('configs')->where('id', 1)->value('email'),
                    'sendername'         =>   DB::table('configs')->where('id', 1)->value('name'),
                    'template'           =>   "email.order"
                ];

                Mail::send('email.order',$data, function($message) use ($data){
                    $message->to($data['sender'],$data['sendername'])->subject($data['subject']);
                    $message->from($data['sender'],$data['sendername']);
                });

                Mail::send('email.client',$data, function($message) use ($data){
                    $message->to($data['to'],$data['name'])->subject($data['subject']);
                    $message->from($data['sender'],$data['sendername']);
                });
                return redirect('/login')->withSuccess('Payment made successfully, kindly proceed to login.');
            }      
    }
}