<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    
    protected $table = 'invoices';

    protected $fillable = [
        'invoice_no', 'writer_id', 'orders', 'total', 'status'
    ];    
    public function writers()
    {
        return $this->hasOne('App\User','id','writer_id');
    } 
}
