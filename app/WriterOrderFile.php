<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WriterOrderFile extends Model
{
    use SoftDeletes;
    
    protected $table = 'writer_order_files';

    protected $fillable = [
        'order_id', 'file_name'
    ];
}
