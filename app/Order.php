<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    
    protected $table = 'orders';

    protected $fillable = [
        'client_id', 'writer_id', 'topic', 'subject', 'words',
        'academic_level', 'sources', 'referencing_style', 'instructions',
        'urgency', 'currency', 'amount', 'status', 'deadline', 'deadline_writers',
        'preferred_writer', 'writer_paid', 'writer_pay', 'order_period'
    ];
        
    public function orderFiles()
    {
        return $this->hasMany('App\OrderFile', 'order_id');
    }    
    public function subjects()
    {
        return $this->hasOne('App\Subject','id','subject');
    }    
    public function clients()
    {
        return $this->hasOne('App\User','id','client_id');
    }

}
