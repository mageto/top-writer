<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## About Top Writer

A Web Portal for writers with direct clients and who manage their own writers.


## Technologies Used

Laravel PHP framework is the one that was used to manage both front end and back end. In the near future we can have the front end done on react.

## Contributors

The major contributor is Mageto Denis [Mageto Denis](http:magetonyarondia.com).
