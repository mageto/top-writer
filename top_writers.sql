-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 03, 2019 at 08:06 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `top_writers`
--
CREATE DATABASE IF NOT EXISTS `top_writers` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `top_writers`;

-- --------------------------------------------------------

--
-- Table structure for table `academic_level`
--

CREATE TABLE `academic_level` (
  `id` int(10) UNSIGNED NOT NULL,
  `pvalue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `academic_level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `academic_level`
--

INSERT INTO `academic_level` (`id`, `pvalue`, `academic_level`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1.0', 'highschool', 'High School', NULL, NULL, NULL),
(2, '1.113', 'undergraduate', 'Undergraduate', NULL, NULL, NULL),
(3, '1.17', 'master', 'Master', NULL, NULL, NULL),
(4, '1.335', 'doctoral', 'Doctoral', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `name`, `website`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Top Writers', '127.0.0.1:8000', 'info@nsig.co.ke', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_prices`
--

CREATE TABLE `custom_prices` (
  `id` int(20) NOT NULL,
  `deadline` varchar(20) NOT NULL,
  `highschool` varchar(20) NOT NULL,
  `undergraduate` varchar(20) NOT NULL,
  `master` varchar(20) NOT NULL,
  `doctoral` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_prices`
--

INSERT INTO `custom_prices` (`id`, `deadline`, `highschool`, `undergraduate`, `master`, `doctoral`) VALUES
(1, '3 hours', '18', '20', '21', '24'),
(2, '6 hours', '16', '19', '20', '22'),
(3, '8 hours', '15', '18', '19', '21'),
(4, '12 hours', '14', '17', '18', '20'),
(5, '24 hours', '13', '16', '17', '19'),
(6, '3 days', '12', '15', '16', '18'),
(7, '4 days', '11', '14', '15', '17'),
(8, '5 days', '10', '13', '14', '16'),
(9, '7 days', '9.5', '12', '13', '15'),
(10, '10 days', '9', '10', '12', '14'),
(11, '20 days', '8.5', '9', '10', '13'),
(12, '30 days', '8', '8.5', '9', '12');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `thread_id`, `user_id`, `body`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Your work was done', '2019-04-26 10:19:15', '2019-04-26 10:19:15', NULL),
(2, 1, 3, 'How much does it cost?', '2019-04-26 10:20:59', '2019-04-26 10:20:59', NULL),
(3, 1, 1, '50 USD Only', '2019-04-26 12:52:09', '2019-04-26 12:52:09', NULL),
(4, 2, 3, 'Hey how are you', '2019-04-26 13:10:58', '2019-04-26 13:10:58', NULL),
(5, 3, 3, 'Integrated Software Application', '2019-04-26 13:14:45', '2019-04-26 13:14:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(34, '2014_10_12_000000_create_users_table', 1),
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2018_10_06_085233_create_configs_table', 1),
(37, '2018_10_06_090349_create_orders_table', 1),
(38, '2018_10_06_092405_create_order_files_table', 1),
(39, '2018_10_06_092828_create_payments_table', 1),
(40, '2018_10_06_092913_create_smtp_configs_table', 1),
(41, '2018_10_06_092951_create_subjects_table', 1),
(42, '2018_10_06_093047_create_service_types_table', 1),
(43, '2018_10_06_093149_create_writer_ratings_table', 1),
(44, '2018_11_15_152608_create_academic_level_table', 2),
(45, '2018_11_15_154125_create_urgency_table', 3),
(46, '2014_10_28_175635_create_threads_table', 4),
(47, '2014_10_28_175710_create_messages_table', 4),
(48, '2014_10_28_180224_create_participants_table', 4),
(49, '2014_11_03_154831_add_soft_deletes_to_participants_table', 4),
(50, '2014_12_04_124531_add_softdeletes_to_threads_table', 4),
(51, '2017_03_30_152742_add_soft_deletes_to_messages_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(255) NOT NULL,
  `writer_id` int(255) DEFAULT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `words` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `academic_level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sources` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referencing_style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urgency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deadline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deadline_writers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferred_writer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `writer_paid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `writer_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `client_id`, `writer_id`, `topic`, `subject`, `words`, `academic_level`, `sources`, `referencing_style`, `instructions`, `urgency`, `currency`, `amount`, `status`, `deadline`, `deadline_writers`, `preferred_writer`, `writer_paid`, `writer_pay`, `order_period`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 6, 2, 'tsergyers', '2', '3', 'undergraduate', '4', 'HAVARD', 'sgsadfgsdgsdf', '12 hours', '1', '51', 'Complete', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 10:49:11', '2019-04-30 18:35:07'),
(2, 7, NULL, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 15:51:15', '2019-04-17 15:51:15'),
(3, 9, NULL, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 15:52:59', '2019-04-17 15:52:59'),
(4, 11, NULL, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 16:27:25', '2019-04-17 16:27:25'),
(5, 6, 2, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Complete', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 16:28:15', '2019-04-17 16:28:15'),
(6, 14, NULL, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 17:00:01', '2019-04-17 17:00:01'),
(7, 16, NULL, 'gsdfgsdfg', '3', '1', 'undergraduate', '3', 'HAVARD', 'dfsgfgsd', '3 hours', '1', '20', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-17 17:00:38', '2019-04-17 17:00:38'),
(8, 12, NULL, 'Website', '15', '3', 'undergraduate', '4', 'MLA', 'I decided to make a personal portfolio on the school project because it would capture all the aspects that I have learnt this semester. The whole concept of doing this project is to', '3 days', '1', '45', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-18 10:38:57', '2019-04-18 10:38:57'),
(9, 6, NULL, 'Email Test', '17', '4', 'master', '3', 'MLA', 'Kindly read the below attached documents for everything you need.', '12 hours', '1', '72', 'Deleted', '2019-04-27 09:43:05', NULL, NULL, 'unpaid', NULL, NULL, '2019-05-03 07:41:31', '2019-04-27 06:43:05', '2019-05-03 07:41:31'),
(10, 6, NULL, 'Email Test', '17', '4', 'master', '3', 'MLA', 'Kindly read the below attached documents for everything you need.', '12 hours', '1', '72', 'Canceled', '2019-04-27 09:48:45', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 06:48:45', '2019-05-03 07:46:34'),
(11, 6, NULL, 'Email Test', '17', '4', 'master', '3', 'MLA', 'Kindly read the below attached documents for everything you need.', '12 hours', '1', '72', 'Canceled', '2019-04-27 09:52:50', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 06:52:50', '2019-05-03 07:47:57'),
(12, 6, NULL, 'Email Test', '17', '4', 'master', '3', 'MLA', 'Kindly read the below attached documents for everything you need.', '12 hours', '1', '72', 'Canceled', '2019-04-27 09:58:39', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 06:58:39', '2019-05-03 07:49:08'),
(13, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Canceled', '2019-04-27 10:18:29', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:18:29', '2019-05-03 07:50:20'),
(14, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Canceled', '2019-04-27 10:22:39', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:22:39', '2019-05-03 07:52:19'),
(15, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:23:32', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:23:32', '2019-04-27 07:23:32'),
(16, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:35:46', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:35:46', '2019-04-27 07:35:46'),
(17, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:37:09', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:37:09', '2019-04-27 07:37:09'),
(18, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:39:46', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:39:46', '2019-04-27 07:39:46'),
(19, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:40:49', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:40:49', '2019-04-27 07:40:49'),
(20, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:43:21', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:43:21', '2019-04-27 07:43:21'),
(21, 6, NULL, 'Email Test', '7', '4', 'master', '4', 'HAVARD', 'Kindly check the attached files for the required instructions.', '12 hours', '1', '72', 'Open', '2019-04-27 10:45:18', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 07:45:18', '2019-04-27 07:45:18'),
(22, 6, NULL, 'Email Test', '4', '4', 'master', '5', 'HAVARD', 'asdfghjklkjhgfds', '8 hours', '1', '76', 'Open', '2019-04-27 11:01:26', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 08:01:26', '2019-04-27 08:01:26'),
(23, 6, NULL, 'Email Test', '4', '4', 'master', '5', 'HAVARD', 'asdfghjklkjhgfds', '8 hours', '1', '76', 'Open', '2019-04-27 11:02:04', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-27 08:02:04', '2019-04-27 08:02:04'),
(24, 6, NULL, 'Email Test', '10', '3', 'undergraduate', '4', 'HAVARD', 'afsafdsfasfasfas', '12 hours', '1', '51', 'Open', '2019-04-28 06:12:06', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-28 03:12:06', '2019-04-28 03:12:06'),
(25, 6, NULL, 'Email Test', '10', '3', 'undergraduate', '4', 'HAVARD', 'afsafdsfasfasfas', '12 hours', '1', '51', 'Open', '2019-04-28 06:12:41', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-28 03:12:41', '2019-04-28 03:12:41'),
(26, 6, NULL, 'Email Test', '10', '3', 'undergraduate', '4', 'HAVARD', 'afsafdsfasfasfas', '12 hours', '1', '51', 'Open', '2019-04-28 06:20:31', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-28 03:20:31', '2019-04-28 03:20:31'),
(27, 6, NULL, 'Email Test', '2', '1', 'highschool', '3', 'HAVARD', 'asdfghjk', '3 hours', '1', '18', 'Open', '2019-04-29 04:52:22', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 01:52:22', '2019-04-29 01:52:22'),
(28, 6, NULL, 'Email Test', '2', '1', 'highschool', '3', 'HAVARD', 'asdfghjk', '3 hours', '1', '18', 'Open', '2019-04-29 05:05:15', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:05:15', '2019-04-29 02:05:15'),
(29, 6, NULL, 'Email Test', '3', '3', 'master', '3', 'HAVARD', 'dfgsdfgsdfg', '3 hours', '1', '63', 'Open', '2019-04-29 05:09:50', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:09:50', '2019-04-29 02:09:50'),
(30, 6, NULL, 'Email Test', '3', '3', 'master', '3', 'HAVARD', 'dfgsdfgsdfg', '3 hours', '1', '63', 'Open', '2019-04-29 05:12:21', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:12:21', '2019-04-29 02:12:21'),
(31, 6, NULL, 'I am title', '3', '5', 'doctoral', '3', 'HAVARD', 'adfsfasdfasdfa', '3 hours', '1', '120', 'Open', '2019-04-29 05:18:19', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:18:19', '2019-04-29 02:18:19'),
(32, 6, NULL, 'I am title', '3', '3', 'undergraduate', '3', 'HAVARD', 'dfgsfgsdfgfdsfgsdfgd', '8 hours', '1', '54', 'Open', '2019-04-29 05:24:35', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:24:35', '2019-04-29 02:24:35'),
(33, 6, NULL, 'I am title', '3', '3', 'undergraduate', '3', 'HAVARD', 'dfgsfgsdfgfdsfgsdfgd', '8 hours', '1', '54', 'Open', '2019-04-29 05:26:03', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:26:03', '2019-04-29 02:26:03'),
(34, 6, NULL, 'tsergyers', '2', '5', 'doctoral', '4', 'HAVARD', 'dsfsgsdgsdfgdsfg', '3 hours', '1', '120', 'Open', '2019-04-29 05:31:18', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:31:18', '2019-04-29 02:31:18'),
(35, 6, NULL, 'sgsfdgsdf', '4', '1', 'master', '3', 'MLA', 'dsfsdfsd', '3 hours', '1', '21', 'Open', '2019-04-29 05:33:50', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:33:50', '2019-04-29 02:33:50'),
(36, 6, NULL, 'Website', '2', '5', 'master', '3', 'HAVARD', 'asdfghjk', '6 hours', '1', '100', 'Open', '2019-04-29 05:37:39', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-04-29 02:37:39', '2019-04-29 02:37:39'),
(37, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', '2019-05-03 17:20:49', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:20:49', '2019-05-03 14:20:49'),
(38, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', '2019-05-03 17:23:29', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:23:29', '2019-05-03 14:23:29'),
(39, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Open', '2019-05-03 17:25:44', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:25:44', '2019-05-03 14:25:44'),
(40, 6, NULL, 'Paypal Test', '1', '3', 'master', '2', 'HAVARD', 'adfasdfasdfasdfasdfasdf', '8 hours', '1', '57', 'Open', '2019-05-03 17:32:10', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:32:10', '2019-05-03 14:32:10'),
(41, 6, NULL, 'Paypal Test', '1', '3', 'master', '2', 'HAVARD', 'adfasdfasdfasdfasdfasdf', '8 hours', '1', '57', 'Open', '2019-05-03 17:32:46', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:32:46', '2019-05-03 14:32:46'),
(42, 6, NULL, 'Email Test', '2', '4', 'undergraduate', '3', 'MLA', 'sfasdfasdfasd', '12 hours', '1', '68', 'Open', '2019-05-03 17:34:39', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:34:39', '2019-05-03 14:34:39'),
(43, 6, NULL, 'Paypal Test', '6', '2', 'undergraduate', '3', 'HAVARD', 'vsadfasdfsadf', '6 hours', '1', '38', 'Open', '2019-05-03 17:36:35', NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:36:35', '2019-05-03 14:36:35'),
(44, 19, NULL, 'Paypal New Test', '1', '2', 'master', '2', 'MLA', 'dsfsdfasdfasd', '3 hours', '1', '42', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:49:40', '2019-05-03 14:49:40'),
(45, 21, NULL, 'Paypal New Test', '1', '2', 'master', '2', 'MLA', 'dsfsdfasdfasd', '3 hours', '1', '42', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:52:32', '2019-05-03 14:52:32'),
(46, 22, NULL, 'Paypal New Test', '2', '2', 'undergraduate', '4', 'MLA', 'sfasfasdfasdf', '8 hours', '1', '36', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:54:27', '2019-05-03 14:54:27'),
(47, 23, NULL, 'Paypal Test New', '4', '6', 'undergraduate', '3', 'HAVARD', 'dsfasdfadfasdfasdf', '12 hours', '1', '102', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 14:57:48', '2019-05-03 14:57:48'),
(48, 24, NULL, 'Paypal Test New', '4', '6', 'undergraduate', '3', 'HAVARD', 'dsfasdfadfasdfasdf', '12 hours', '1', '102', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 15:01:06', '2019-05-03 15:01:06'),
(49, 25, NULL, 'Email Test', '1', '4', 'undergraduate', '2', 'MLA', 'dfgdsgasdgsadg', '8 hours', '1', '72', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 15:04:30', '2019-05-03 15:04:30'),
(50, 26, NULL, 'Paypal Final', '2', '3', 'undergraduate', '2', 'HAVARD', 'sasfdasdfasdfasd', '8 hours', '1', '54', 'Open', NULL, NULL, NULL, 'unpaid', NULL, NULL, NULL, '2019-05-03 15:11:51', '2019-05-03 15:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `order_files`
--

CREATE TABLE `order_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_files`
--

INSERT INTO `order_files` (`id`, `order_id`, `file_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1555527179-3-Cover Letter.pdf', NULL, '2019-04-17 15:53:00', '2019-04-17 15:53:00'),
(2, 1, '1555527180-3-cv1.pdf', NULL, '2019-04-17 15:53:00', '2019-04-17 15:53:00'),
(3, 3, '1555527180-3-Resume.pdf', NULL, '2019-04-17 15:53:00', '2019-04-17 15:53:00'),
(4, 4, '1555529245-4-Cover Letter.pdf', NULL, '2019-04-17 16:27:25', '2019-04-17 16:27:25'),
(5, 4, '1555529246-4-cv1.pdf', NULL, '2019-04-17 16:27:26', '2019-04-17 16:27:26'),
(6, 4, '1555529246-4-Resume.pdf', NULL, '2019-04-17 16:27:26', '2019-04-17 16:27:26'),
(7, 5, '1555529295-5-Cover Letter.pdf', NULL, '2019-04-17 16:28:15', '2019-04-17 16:28:15'),
(8, 5, '1555529295-5-cv1.pdf', NULL, '2019-04-17 16:28:15', '2019-04-17 16:28:15'),
(9, 5, '1555529296-5-Resume.pdf', NULL, '2019-04-17 16:28:16', '2019-04-17 16:28:16'),
(10, 6, '1555531201-6-Cover Letter.pdf', NULL, '2019-04-17 17:00:01', '2019-04-17 17:00:01'),
(11, 6, '1555531202-6-Cover Letter.docx', NULL, '2019-04-17 17:00:02', '2019-04-17 17:00:02'),
(12, 6, '1555531202-6-c.docx', NULL, '2019-04-17 17:00:02', '2019-04-17 17:00:02'),
(13, 7, '1555531238-7-Cover Letter.pdf', NULL, '2019-04-17 17:00:38', '2019-04-17 17:00:38'),
(14, 7, '1555531238-7-Cover Letter.docx', NULL, '2019-04-17 17:00:38', '2019-04-17 17:00:38'),
(15, 7, '1555531238-7-c.docx', NULL, '2019-04-17 17:00:38', '2019-04-17 17:00:38'),
(16, 8, '1555594739-8-c.docx', NULL, '2019-04-18 10:39:00', '2019-04-18 10:39:00'),
(17, 8, '1555594740-8-CV.docx', NULL, '2019-04-18 10:39:00', '2019-04-18 10:39:00'),
(18, 8, '1555594740-8-Cover Letter.docx', NULL, '2019-04-18 10:39:01', '2019-04-18 10:39:01'),
(19, 9, '1556358186-9-Resume.pdf', NULL, '2019-04-27 06:43:07', '2019-04-27 06:43:07'),
(20, 9, '1556358187-9-Resume.pdf', NULL, '2019-04-27 06:43:07', '2019-04-27 06:43:07'),
(21, 10, '1556358525-10-Resume.pdf', NULL, '2019-04-27 06:48:45', '2019-04-27 06:48:45'),
(22, 10, '1556358525-10-Resume.pdf', NULL, '2019-04-27 06:48:45', '2019-04-27 06:48:45'),
(23, 11, '1556358770-11-Resume.pdf', NULL, '2019-04-27 06:52:50', '2019-04-27 06:52:50'),
(24, 11, '1556358770-11-Resume.pdf', NULL, '2019-04-27 06:52:50', '2019-04-27 06:52:50'),
(25, 12, '1556359119-12-Resume.pdf', NULL, '2019-04-27 06:58:40', '2019-04-27 06:58:40'),
(26, 12, '1556359120-12-Resume.pdf', NULL, '2019-04-27 06:58:40', '2019-04-27 06:58:40'),
(27, 13, '1556360309-13-cv1.pdf', NULL, '2019-04-27 07:18:30', '2019-04-27 07:18:30'),
(28, 14, '1556360560-14-cv1.pdf', NULL, '2019-04-27 07:22:42', '2019-04-27 07:22:42'),
(29, 15, '1556360613-15-cv1.pdf', NULL, '2019-04-27 07:23:33', '2019-04-27 07:23:33'),
(30, 16, '1556361346-16-cv1.pdf', NULL, '2019-04-27 07:35:47', '2019-04-27 07:35:47'),
(31, 17, '1556361429-17-cv1.pdf', NULL, '2019-04-27 07:37:09', '2019-04-27 07:37:09'),
(32, 18, '1556361586-18-cv1.pdf', NULL, '2019-04-27 07:39:46', '2019-04-27 07:39:46'),
(33, 19, '1556361649-19-cv1.pdf', NULL, '2019-04-27 07:40:49', '2019-04-27 07:40:49'),
(34, 20, '1556361801-20-cv1.pdf', NULL, '2019-04-27 07:43:21', '2019-04-27 07:43:21'),
(35, 21, '1556361918-21-cv1.pdf', NULL, '2019-04-27 07:45:18', '2019-04-27 07:45:18'),
(36, 23, '1556362925-23-cv1.pdf', NULL, '2019-04-27 08:02:05', '2019-04-27 08:02:05'),
(37, 24, '1556431926-24-Resume.pdf', NULL, '2019-04-28 03:12:06', '2019-04-28 03:12:06'),
(38, 25, '1556431961-25-Resume.pdf', NULL, '2019-04-28 03:12:41', '2019-04-28 03:12:41'),
(39, 26, '1556432432-26-Resume.pdf', NULL, '2019-04-28 03:20:32', '2019-04-28 03:20:32'),
(40, 27, '1556513542-27-cv1.pdf', NULL, '2019-04-29 01:52:22', '2019-04-29 01:52:22'),
(41, 28, '1556514315-28-cv1.pdf', NULL, '2019-04-29 02:05:15', '2019-04-29 02:05:15'),
(42, 29, '1556514591-29-Mageto Denis Resume.pdf', NULL, '2019-04-29 02:09:51', '2019-04-29 02:09:51'),
(43, 30, '1556514741-30-Mageto Denis Resume.pdf', NULL, '2019-04-29 02:12:21', '2019-04-29 02:12:21'),
(44, 31, '1556515099-31-cv1.pdf', NULL, '2019-04-29 02:18:19', '2019-04-29 02:18:19'),
(45, 32, '1556515475-32-cv1.pdf', NULL, '2019-04-29 02:24:35', '2019-04-29 02:24:35'),
(46, 33, '1556515563-33-cv1.pdf', NULL, '2019-04-29 02:26:03', '2019-04-29 02:26:03'),
(47, 34, '1556515878-34-cv1.pdf', NULL, '2019-04-29 02:31:18', '2019-04-29 02:31:18'),
(48, 35, '1556516031-35-CV.pdf', NULL, '2019-04-29 02:33:51', '2019-04-29 02:33:51'),
(49, 36, '1556516259-36-CV.pdf', NULL, '2019-04-29 02:37:40', '2019-04-29 02:37:40'),
(50, 41, '1556904766-41-CV.pdf', NULL, '2019-05-03 14:32:47', '2019-05-03 14:32:47'),
(51, 42, '1556904879-42-CV.docx', NULL, '2019-05-03 14:34:39', '2019-05-03 14:34:39'),
(52, 43, '1556904995-43-Cover Letter.pdf', NULL, '2019-05-03 14:36:35', '2019-05-03 14:36:35'),
(53, 44, '1556905780-44-Cover Letter.pdf', NULL, '2019-05-03 14:49:40', '2019-05-03 14:49:40'),
(54, 45, '1556905952-45-Cover Letter.pdf', NULL, '2019-05-03 14:52:33', '2019-05-03 14:52:33'),
(55, 46, '1556906067-46-cv1.pdf', NULL, '2019-05-03 14:54:27', '2019-05-03 14:54:27'),
(56, 47, '1556906269-47-Cover Letter.pdf', NULL, '2019-05-03 14:57:49', '2019-05-03 14:57:49'),
(57, 48, '1556906466-48-Cover Letter.pdf', NULL, '2019-05-03 15:01:06', '2019-05-03 15:01:06'),
(58, 49, '1556906670-49-Cover Letter.pdf', NULL, '2019-05-03 15:04:30', '2019-05-03 15:04:30'),
(59, 50, '1556907111-50-Denis mageto.docx', NULL, '2019-05-03 15:11:51', '2019-05-03 15:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `last_read` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `thread_id`, `user_id`, `last_read`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2019-04-26 12:53:05', '2019-04-26 10:19:15', '2019-04-26 12:53:05', NULL),
(2, 1, 3, '2019-04-26 13:07:28', '2019-04-26 10:19:15', '2019-04-26 13:07:28', NULL),
(3, 2, 3, '2019-04-26 13:10:59', '2019-04-26 13:10:59', '2019-04-26 13:10:59', NULL),
(4, 2, 1, NULL, '2019-04-26 13:10:59', '2019-04-26 13:10:59', NULL),
(5, 3, 3, '2019-04-26 13:15:00', '2019-04-26 13:14:45', '2019-04-26 13:15:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `service_types`
--

CREATE TABLE `service_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smtp_configs`
--

CREATE TABLE `smtp_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `smtp_host` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protocol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Accounting', NULL, NULL, NULL),
(2, 'Archaeology', NULL, NULL, NULL),
(3, 'Architecture', NULL, NULL, NULL),
(4, 'Arts', NULL, NULL, NULL),
(5, 'Astronomy', NULL, NULL, NULL),
(6, 'Biology', NULL, NULL, NULL),
(7, 'Business', NULL, NULL, NULL),
(8, 'Chemistry', NULL, NULL, NULL),
(9, 'Childcare', NULL, NULL, NULL),
(10, 'Computers', NULL, NULL, NULL),
(11, 'Counseling', NULL, NULL, NULL),
(12, 'Criminology', NULL, NULL, NULL),
(13, 'Economics', NULL, NULL, NULL),
(14, 'Education', NULL, NULL, NULL),
(15, 'Engineering', NULL, NULL, NULL),
(16, 'Environmental-Studies', NULL, NULL, NULL),
(17, 'Ethics', NULL, NULL, NULL),
(18, 'Ethnic-Studies', NULL, NULL, NULL),
(19, 'Finance', NULL, NULL, NULL),
(20, 'Food-Nutrition', NULL, NULL, NULL),
(21, 'Geography', NULL, NULL, NULL),
(22, 'Healthcare', NULL, NULL, NULL),
(23, 'History', NULL, NULL, NULL),
(24, 'Law', NULL, NULL, NULL),
(25, 'Linguistics', NULL, NULL, NULL),
(26, 'Literature', NULL, NULL, NULL),
(27, 'Management', NULL, NULL, NULL),
(28, 'Marketing', NULL, NULL, NULL),
(29, 'Mathematics', NULL, NULL, NULL),
(30, 'Medicine', NULL, NULL, NULL),
(31, 'Music', NULL, NULL, NULL),
(32, 'Nursing', NULL, NULL, NULL),
(33, 'Philosophy', NULL, NULL, NULL),
(34, 'Physical-Education', NULL, NULL, NULL),
(35, 'Physics', NULL, NULL, NULL),
(36, 'Political-Science', NULL, NULL, NULL),
(37, 'Programming', NULL, NULL, NULL),
(38, 'Psychology', NULL, NULL, NULL),
(39, 'Religion', NULL, NULL, NULL),
(40, 'Sociology', NULL, NULL, NULL),
(41, 'Statistics', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `subject`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Work Done', '2019-04-26 10:19:15', '2019-04-26 12:52:09', NULL),
(2, 'Education Rwanda', '2019-04-26 13:10:58', '2019-04-26 13:10:58', NULL),
(3, 'Integrated Software Application', '2019-04-26 13:14:44', '2019-04-26 13:14:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `urgency`
--

CREATE TABLE `urgency` (
  `id` int(10) UNSIGNED NOT NULL,
  `pvalue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urgency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `urgency`
--

INSERT INTO `urgency` (`id`, `pvalue`, `urgency`, `duration`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '18', '3', 'Hours', NULL, NULL, NULL),
(2, '16', '6', 'Hours', NULL, NULL, NULL),
(3, '15', '8', 'Hours', NULL, NULL, NULL),
(4, '14', '12', 'Hours', NULL, NULL, NULL),
(5, '13', '12-24', 'Hours', NULL, NULL, NULL),
(6, '12', '18-3', 'Hours-Days', NULL, NULL, NULL),
(7, '11', '24-4', 'Hours-Days', NULL, NULL, NULL),
(8, '10', '3-5', 'Days', NULL, NULL, NULL),
(9, '9.5', '5-7', 'Days', NULL, NULL, NULL),
(10, '9.0', '7-10', 'Days', NULL, NULL, NULL),
(11, '8.5', '10-20', 'Days', NULL, NULL, NULL),
(12, '8', '30', 'Days', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'client',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `gender`, `avatar`, `user_type`, `address`, `phone`, `status`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admins@gmail.com', '$2y$10$iYWujx5TZfKtKyfFxdU2.OyRC/2j.RH07IYJy5ZaiBTN.SFWPOBOa', 'Male', '1556888993-1-111.jpg', 'admin', '47773-00100', '0711234567', '1', NULL, 'BepsrutVaweYRZ4X9f289Xfev8BjBdBnX4onu8wseMmHfWdAJaXqn5AG5ZQq', '2018-11-15 09:32:21', '2019-05-03 10:28:07'),
(2, 'Writer', 'writer@gmail.com', '$2y$10$mg5b.bOIwe3CfMkDnn4d1OsZ8qdOuonaU14Fpn.tOOFKyvx6fr5MG', NULL, NULL, 'writer', NULL, NULL, '1', NULL, 'eBXnHeWDU6j5eImQHiX6c40JI4NeXVVbtsX10S1JkNMmvUJ9ljOyavv02wxz', '2018-11-15 12:00:40', '2018-11-15 12:00:40'),
(3, 'Client', 'client@gmail.com', '$2y$10$JsLu2YFbyFWUl9bHxBP8LedOdL8m6ZxUuqGm3PTtU20jxHtO9nuja', NULL, NULL, 'client', NULL, NULL, '1', NULL, 'fSN2AGSP8z4l3aNz01GCndyGCs29UafqdhgSxUSKCFeMZVVAlKPZt8iKtfod', '2018-11-15 12:03:32', '2018-11-15 12:03:32'),
(4, 'Mageto Denis', 'mageto.denigs@gmail.com', '$2y$10$PGe1r6tKab3V3Sl9lEqSf.OLrKUMladKWtJRBp/8SjkOeehoATxp2', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-04-17 10:47:59', '2019-04-17 10:47:59'),
(6, 'Email Test', 'mageto.denism@gmail.com', '$2y$10$ts94iPHy6XU6ZUhAnUsIB.JfFVQKnrsx.JVcTReJLU98QzxpymN5q', NULL, NULL, 'client', NULL, NULL, '1', NULL, 'TwEVTbATpOZI3Ig6RIHknxPKkN4VYWrARPq42DDd9SuqgprxxVFbSLlvC50y', '2019-04-17 10:49:11', '2019-04-17 10:49:11'),
(7, 'Denis A', 'admin1@example.com', '$2y$10$bssEYG7ny.eAVIJcTs0Rtupz1rhNWD588HfK7FNoDO8hdWCA9BWRe', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-04-17 15:51:14', '2019-04-17 15:51:14'),
(9, 'Denis B', 'admin2@example.com', '$2y$10$gR5mkUFtJUA/wH2xOk2A6Op9Q9NyBtH06Wxkrl.Uj1KL4TXRFlrDK', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-04-17 15:52:59', '2019-04-17 15:52:59'),
(11, 'Denis C', 'admin3@example.com', '$2y$10$1lD6UHr3a5ilRWbBNjrtAuNw8oB0B0c1jpAi.FxWT/ZbRs30MMZWO', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-04-17 16:27:25', '2019-04-17 16:27:25'),
(12, 'Denis D', 'admin@example.com', '$2y$10$iYWujx5TZfKtKyfFxdU2.OyRC/2j.RH07IYJy5ZaiBTN.SFWPOBOa', NULL, NULL, 'client', NULL, NULL, '1', NULL, 'avAWjh359iEVjB6aejUyx4XxXQUrcLehz9DEjiLMAqiXwFSS3lGOt9AgCdZP', '2019-04-17 16:28:15', '2019-04-17 16:28:15'),
(14, 'Denis E', 'admind@gmail.com', '$2y$10$4.pxuyI2bQrcs2/Xx1.PxuF3IhmQm8Xl/2lrMzeuS7fS35xFD21Yu', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-04-17 17:00:01', '2019-04-17 17:00:01'),
(16, 'Denis F', 'admin@gmail.com', '$2y$10$n1Ade57oKEjYfgFiarypeu6FET0lhfFDbX9SHLgQsQxEM5GKJ9j/q', NULL, NULL, 'client', NULL, NULL, '1', NULL, 'T1sBMze0XqQFatPlmrEgE9L4pUkmN9uo8x0B9KIl1xnkMOCb5JglWmBXLs2m', '2019-04-17 17:00:38', '2019-04-17 17:00:38'),
(18, 'Mageto Denis', 'denis@gmail.com', '$2y$10$Atbv3V1rKmkvJlpOTDI9iu1Hc692lDl7b7PA8k46/bADC532QpJjW', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 07:56:32', '2019-05-03 07:56:32'),
(19, 'Mageto Denis', 'mageto.d@gmail.com', '$2y$10$CzcjVjDMb4YFnz7QlUo25O6mKmP12qY2rDzDAVe.WoNmqx5ZwY3K6', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 14:49:40', '2019-05-03 14:49:40'),
(21, 'Mageto Denis', 'mageto.de@gmail.com', '$2y$10$Q7ImxuQsWhDRXk6GPRpuFe9GlN.tHE3V2dgUHjNjIfy/st4n0/U.C', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 14:52:32', '2019-05-03 14:52:32'),
(22, 'Mageto Denis', 'mageto.deni@gmail.com', '$2y$10$c72KElD8HGriVv7w9o57lOhiRE5Zk7URF03QDEfIAtPFbw1onOHtG', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 14:54:27', '2019-05-03 14:54:27'),
(23, 'Mageto Denis', 'mageto.deis@gmail.com', '$2y$10$NrJ1BLtvnLqfyZRfto6k9.Fyf93gokzKVlwaxZjfV663M7Xc.OoqO', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 14:57:48', '2019-05-03 14:57:48'),
(24, 'Mageto Denis', 'mageto.nis@gmail.com', '$2y$10$EL1yikSgi0f.ISPAzuYDfObNKK7gDT.F2T6AC3xelancrQmGctiBW', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 15:01:05', '2019-05-03 15:01:05'),
(25, 'Mageto Denis', 'mage.denis@gmail.com', '$2y$10$OtjTaOSnrXuILuLjCXFuYeqRQ1QxYCW.x7fqO3m7po7QzDb4cSSaK', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 15:04:30', '2019-05-03 15:04:30'),
(26, 'Mageto Denis', 'mageto.denis@gmail.com', '$2y$10$RBfPgpIatKqcHanIRUfzBOzYEiZ3EaWnd8AqAOXrhUaSq4n86dlQ.', NULL, NULL, 'client', NULL, NULL, '1', NULL, NULL, '2019-05-03 15:11:51', '2019-05-03 15:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `writer_order_files`
--

CREATE TABLE `writer_order_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `writer_order_files`
--

INSERT INTO `writer_order_files` (`id`, `order_id`, `file_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1556876731-1-Resume.pdf', NULL, '2019-05-03 06:45:31', '2019-05-03 06:45:31'),
(2, 1, '1556876731-1-cv1.pdf', NULL, '2019-05-03 06:45:31', '2019-05-03 06:45:31');

-- --------------------------------------------------------

--
-- Table structure for table `writer_ratings`
--

CREATE TABLE `writer_ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `writer_id` int(11) NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_level`
--
ALTER TABLE `academic_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_prices`
--
ALTER TABLE `custom_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_files`
--
ALTER TABLE `order_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_types`
--
ALTER TABLE `service_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smtp_configs`
--
ALTER TABLE `smtp_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `urgency`
--
ALTER TABLE `urgency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `writer_order_files`
--
ALTER TABLE `writer_order_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `writer_ratings`
--
ALTER TABLE `writer_ratings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_level`
--
ALTER TABLE `academic_level`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `custom_prices`
--
ALTER TABLE `custom_prices`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `order_files`
--
ALTER TABLE `order_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `service_types`
--
ALTER TABLE `service_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smtp_configs`
--
ALTER TABLE `smtp_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `urgency`
--
ALTER TABLE `urgency`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `writer_order_files`
--
ALTER TABLE `writer_order_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `writer_ratings`
--
ALTER TABLE `writer_ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
